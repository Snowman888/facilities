<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('c_id');
            $table->integer('organization_id')->unsigned()->index()->nullable();
            $table->foreign('organization_id')->references('org_id')->on('organizations');
            $table->string('c_first_name');
            $table->string('c_last_name');
            $table->string('c_type')->nullable();            
            $table->string('c_email')->nullable();
            $table->string('c_home')->nullable();
            $table->string('c_cell')->nullable();
            $table->string('c_work')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
};
