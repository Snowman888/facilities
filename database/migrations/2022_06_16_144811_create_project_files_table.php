<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_files', function (Blueprint $table) {
            $table->increments('proj_id');
            $table->integer('project_mjob_id')->unsigned()->index()->nullable();
            $table->foreign('project_mjob_id')->references('mjob_id')->on('master_jobs');
            $table->string('project_id'); 
            $table->string('project_status');            
            $table->string('project_type');
            $table->string('project_manager');
            $table->string('project_lead');
            $table->date('project_date_opened')->nullable();
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_files');
    }
};
