<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('org_id');
            $table->string('org_name')->unique();
            $table->string('org_type');
            $table->string('org_phone')->nullable();
            $table->string('org_fax')->nullable();
            $table->string('org_email')->nullable();
            $table->string('org_address')->nullable();          
            $table->timestamp('deleted_at')->nullable();
        });

        Schema::create('master_jobs', function (Blueprint $table) {
            $table->increments('mjob_id');
            $table->integer('m_creator_id')->unsigned()->index();
            $table->foreign('m_creator_id')->references('id')->on('users');
            $table->string('m_job_id')->unique()->index();
            $table->string('m_status');
            $table->string('m_name');
            $table->string('m_customer_type');
            $table->string('m_c_first_name')->nullable();
            $table->string('m_c_last_name')->nullable();
            $table->integer('organization_id')->unsigned()->index()->nullable();
            $table->foreign('organization_id')->references('org_id')->on('organizations');

            $table->string('m_job_address')->nullable();
            $table->string('m_job_apartment')->nullable();
            $table->string('m_job_city')->nullable();
            $table->string('m_job_state')->nullable();
            $table->string('m_job_zip')->nullable();
            $table->string('m_job_country')->nullable();


            $table->string('m_pay_type');
            $table->string('m_ins_claim')->nullable();
            $table->string('m_ins_policy')->nullable();
            $table->string('m_ins_company')->nullable();
            $table->string('m_ins_deductible')->nullable();

            $table->text('m_loss_desc')->nullable();
            $table->string('m_lead_technician')->nullable();
            $table->string('m_source')->nullable();
            $table->date('m_date_opened');
            $table->string('m_referral')->nullable();
            $table->timestamp('deleted_at')->nullable();

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
        Schema::dropIfExists('master_jobs');
    }
};
