<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipments', function (Blueprint $table) {
            $table->increments('equip_id');
            $table->string('equip_e_id'); 
            $table->string('equip_status');            
            $table->string('equip_type')->nullable();
            $table->string('equip_mfg')->nullable();
            $table->string('equip_model')->nullable();
            $table->string('equip_serial')->nullable();
            $table->date('equip_p_date')->nullable();            
            $table->string('equip_p_cost')->nullable();
            $table->string('equip_vendor')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipments');
    }
};
