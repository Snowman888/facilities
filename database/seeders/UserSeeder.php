<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
   {
      User::factory(1)->create();

      User::create([
    		'first_name' => 'Deepesh',
         'last_name' => 'Thapa',
         'email' => 'deepeshthapa911@gmail.com',
         'logo' => 'demo.jpg',
         'email_verified_at' => now(),
         'password' => \Hash::make('Password123@'),
         'remember_token' => Str::random(10),
		]);
   }
 }
