<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectFile extends Model
{
   use HasFactory,SoftDeletes;

   protected $primaryKey = 'project_id';

   protected $dates = ['deleted_at'];

   protected $guarded = ['project_id','deleted_at','created_at','updated_at'];

   public function masterJob()
   {
      return $this->belongsTo(MasterJob::class,'project_mjob_id');
   }
}
