<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
   use HasFactory,SoftDeletes;

   protected $primaryKey = 'c_id';

   protected $dates = ['deleted_at'];

   protected $guarded = ['c_id','deleted_at'];
   
   public $timestamps = false;

   public function organization()
   {
      return $this->belongsTo(Organization::class,'organization_id');
   }
}
