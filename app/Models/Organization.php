<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model
{
   use HasFactory,SoftDeletes;

   protected $primaryKey = 'org_id';

   protected $dates = ['deleted_at'];

   public $timestamps = false;

   protected $guarded = ['org_id','deleted_at'];

   public function masterJobs()
   {
      return $this->hasMany(MasterJob::class,'organization_id');
   }

   public function contacts()
   {
      return $this->hasMany(Contact::class,'organization_id');
   }
}
