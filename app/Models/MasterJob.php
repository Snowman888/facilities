<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterJob extends Model
{
	use HasFactory,SoftDeletes;

   protected $primaryKey = 'mjob_id';

   protected $dates = ['deleted_at'];

   protected $guarded = [];

   public function creator()
   {
      return $this->belongsTo(User::class,'m_creator_id');
   }

   public function organization()
   {
      return $this->belongsTo(Organization::class,'organization_id');
   }

   public function projects()
   {
      return $this->hasMany(ProjectFile::class,'project_mjob_id');
   }
}
