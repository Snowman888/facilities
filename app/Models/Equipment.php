<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipment extends Model
{
   use HasFactory,SoftDeletes;

   protected $primaryKey = 'equip_id';

   protected $dates = ['deleted_at'];

   protected $guarded = ['equip_id'];

   public $table = "equipments";
}
