<?php

namespace App\Http\Controllers\Backend\Validation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class ParselyValidation extends Controller
{
   public function validate_email(Request $request,$user_id)
	{
		if ($user_id!='new') {
			$user=User::withTrashed()->findOrFail($user_id);
			if ($user->email==$request->value) {
				return response()->json(array('success'=> 'passed'), 200);
			}
		}
		
		$validatore = \Validator::make($request->all(), [
		'value' => 'unique:users,email', 
		]);
		if ($validatore->fails())
		{
			  return response()->json(array('error'=> $validatore), 422);
		} 
		return response()->json(array('success'=> 'passed'), 200);
	}

	public function validateoptions(Request $request)
	{
		//Checking if value is for update. If it is for update
		//The validation will be ignored if same value is sent.

		//send id = new if this is a new record else send the table id

		//table = Main table to search
		//filter_field = Used in Where condition to search for table row/Model
		//id = either new or id of the model to search against
		//table_field = Real field to search for uniqness

		if($request->type=="uniqueness"){
			return $this->uniqueness($request);
		}

		return response()->json(array('error'=> 'error'), 422);
	}

	public function uniqueness($request)
	{	
		if ($request->id!='new') {
			$model=DB::table($request->table)->where($request->filter_field, $request->id)->first();
			if ($model->{$request->table_field}==array_values($request->all())[0]) {
				return response()->json(array('success'=> 'passed'), 200);
			}
		}
		
		$validatore = \Validator::make($request->all(), [
			array_keys($request->all())[0] => 'unique:'.$request->table.','.$request->table_field, 
		]);
		if ($validatore->fails())
		{
			  return response()->json(array('error'=> 'error'), 422);
		} 
		return response()->json(array('success'=> 'passed'), 200);
	}
}
