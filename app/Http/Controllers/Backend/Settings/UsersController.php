<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{
   public function index()
   {
   	$users=User::with("roles")->get();

   	return view('backend.settings.users.index',compact('users'));
   }

   public function create()
   {
   	return view('backend.settings.users.create');
   }

   public function store(Request $request)
   {
   	$roles=\Role::pluck('id')->toArray();

   	$this->validate($request, [
         'logo' => 'nullable|image|max:2500',
         'first_name' => 'required',
         'last_name' => 'required',         
         'email' => 'required|email|unique:users,email',        
         'role' => 'required|in:'.implode(',', $roles), 
         'password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
      ], [
             
      ]);

      $user = new User;      
		$user->password = \Hash::make($request->password);
		$user->email = $request->email;
		$user->first_name = $request->first_name;
      $user->last_name = $request->last_name;
      $user->title = $request->title;
      $user->logo="demo.jpg";
		$user->save();

      if (\Input::hasFile('logo'))
      {
         $path = storage_path().'/app/public/users/' . $user->id;
         $logo = \Input::file('logo');
         $logoname =  $user->id.'.'.$logo->getClientOriginalExtension();
         $logo->move($path, $logoname);
         $user->logo='storage/users/'.$user->id.'/'.$logoname;
      }

      $user->save();

      $role = \Role::findById($request->role);
      $role->users()->attach($user);

      return back()->with('success','User Created');
   }

   public function delete($user_id)
   {
      try {
         User::findOrFail($user_id)->delete(); 
      }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
          return back()->with('error','Oops! Something Went Wrong.');
      }
      

      return back()->with('success','User Deleted');
   }

   public function edit($user_id)
   {
      try {
         $user=User::findOrFail($user_id); 
      }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
          return back()->with('error','Oops! Something Went Wrong.');
      }
      
      return view('backend.settings.users.edit',compact('user'));
   }

   public function update(Request $request,$user_id)
   {
      try {
         $user=User::findOrFail($user_id); 
      }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
          return back()->with('error','Oops! Something Went Wrong.');
      }

      $roles=\Role::pluck('id')->toArray();

      $this->validate($request, [
         'logo' => 'nullable|image|max:2500',
         'first_name' => 'required',
         'last_name' => 'required',         
         'email' => 'required|email|unique:users,email,'.$user_id,      
         'role' => 'required|in:'.implode(',', $roles), 
         'password' => 'nullable|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
      ], [
             
      ]);

      if ($request->password) {
         $user->password = \Hash::make($request->password);
      }
      
      $user->email = $request->email;
      $user->first_name = $request->first_name;
      $user->last_name = $request->last_name;
      $user->title = $request->title;

      if (\Input::hasFile('logo'))
      {
         if($user->logo!='demo.jpg')
         {
             \File::delete($user->logo);
         }
         $path = storage_path().'/app/public/users/' . $user->id;
         $logo = \Input::file('logo');
         $logoname =  $user->id.'.'.$logo->getClientOriginalExtension();
         $logo->move($path, $logoname);
         $user->logo='storage/users/'.$user->id.'/'.$logoname;
      }

      $user->save();

      $user->removeRole($user->roles->first());
      $role = \Role::findById($request->role);
      $role->users()->attach($user);

      return redirect('settings/users')->with('success','User Updated');
   }
}
