<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;

class ProfileController extends Controller
{
   public function index()
   {
   	return view('backend.settings.profile.index');
   }

   public function update(Request $request)
   {
      $this->validate($request, [
         'logo' => 'nullable|image|max:2500',
         'first_name' => 'required',
         'last_name' => 'required',         
         'email' => 'required|email|unique:users,email,'.auth()->user()->id,         
      ], [
             
      ]);

      $user =\Auth::user();
      if (\Input::hasFile('logo'))
      {
         if($user->logo!='demo.jpg')
         {
             \File::delete($user->logo);
         }
         $path = storage_path().'/app/public/users/' . $user->id;
         $logo = \Input::file('logo');
         $logoname =  $user->id.'.'.$logo->getClientOriginalExtension();
         $logo->move($path, $logoname);
         $user->logo='storage/users/'.$user->id.'/'.$logoname;
      }
      $user->first_name = $request->first_name;
      $user->last_name = $request->last_name;
      $user->title = $request->title;
      $user->email = $request->email;
      $user->save();
      return back()->with('success','Profile Updated');
   }

   public function changePassword(Request $request)
   {
      $this->validate($request, [         
         'current_password' => ['required', new MatchOldPassword],
         'password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
         'password_confirmation' => 'required|same:password'

      ], [
        'current_password.required' => 'Your current password is required',
         'password.required' => 'New password field is empty',
         'password.min' => 'New password must be at least 8 characters long',
         'password_confirmation.required' => 'Password-confirmation field is empty',
         'password_confirmation.same' => 'New password confirmation do not match, Please try again',
         'password.regex' => "New Password must contain at least 1 uppercase, 1 lowercase, 1 number and 1 special character"
      ]);



      $user = \Auth::user();
      $user->password = \Hash::make($request->password);
      $user->save();

      return redirect()->back()->with('success', 'Your password has been updated.'); 
   }

   public function delete()
   {
   	$user = \Auth::user();  
   	\Auth::logout();
   	$user->delete();
		
		return \Redirect::route('login');
   }
}
