<?php

namespace App\Http\Controllers\Backend\Core;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;
use DataTables;
use App\Models\Organization;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.core.contact.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function datatables()
    {
        $contacts=Contact::with('organization:org_id,org_name');

		return Datatables::eloquent($contacts)						  
			->addColumn('action', function ($contact) {
					  return $contact->c_id;
				 })
			->addColumn('org', function ($contact) {
				if ($contact->organization) {
					return $contact->organization->org_name;
				} else {
					return null;
				}
				
			})
			->rawColumns(['action'])                   
			->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$orgs=Organization::pluck('org_name','org_id');
        return view('backend.core.contact.create',compact('orgs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       	$this->validate($request, [
	         'c_first_name' => 'required', 
	         'c_last_name' => 'required', 
	         'c_email' => 'nullable|email', 
	         'c_type' => 'nullable|in:'.implode(',', config('master.contacts.type')), 
	    ], [
	             
	    ]);

	    try {
            Contact::create($request->all());
         } catch (Exception $e) {
            return back()->with('error','Oops! Something Went Wrong.');
         }
        
        return redirect('contacts')->with('success','New Contact Added!');
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($c_id)
    {
        $orgs=Organization::pluck('org_name','org_id');
        $contact=Contact::findOrFail($c_id);

        return view('backend.core.contact.edit',compact('orgs','contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $c_id)
    {
    	try {
            $contact=Contact::findOrFail($c_id);
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return redirect()->back()->with('error','Oops! Something Went Wrong.');
        }

        $this->validate($request, [
	         'c_first_name' => 'required', 
	         'c_last_name' => 'required', 
	         'c_email' => 'nullable|email', 
	         'c_type' => 'nullable|in:'.implode(',', config('master.contacts.type')), 
	    ], [
	             
	    ]);

	    $contact = $contact->updateOrCreate(
            ['c_id' => $c_id],
            $request->all()            
        );

        return redirect('contacts')->with('success','Contact Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($c_id)
    {
        try {
            $org=Contact::findOrFail($c_id);
            $org->delete();
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return redirect()->back()->with('error','Oops! Something Went Wrong.');
        }

        return redirect()->back()->with('success','Contact Deleted!');
    }
}
