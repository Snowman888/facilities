<?php

namespace App\Http\Controllers\Backend\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\MasterJob;
use DataTables;
use App\Models\Organization;
use Carbon\Carbon;

class MasterJobsController extends Controller
{
	public function index()
	{
    	return view('backend.core.master.index');
	}

	public function datatables()
	{
		$mJobs=MasterJob::query();

		return Datatables::eloquent($mJobs)						  
			->addColumn('action', function ($mJob) {
					  return $mJob->mjob_id;
				 })
			->editColumn('m_date_opened', function($mJob) {
              return Carbon::createFromFormat('Y-m-d', $mJob->m_date_opened)->format('d M Y');
          })
			->rawColumns(['action'])                   
			->make(true); 
	}

	public function create()
	{
		$orgs=Organization::pluck('org_name','org_id');
		return view('backend.core.master.create',compact('orgs'));
	}

	public function store(Request $request)
	{
		
		$this->validate($request, [
         'm_job_id' => 'required|unique:master_jobs,m_job_id',  
         'm_name' => 'required', 
         'm_status' => 'required', 
         'm_date_opened' => 'date_format:d M Y|required', 
         'm_customer_type' => 'required|in:'.implode(',', config('master.master_jobs.customer_type')), 

         'm_job_address' => 'required',
         'm_job_city' => 'required',
         'm_job_state' => 'required',
         'm_job_zip' => 'required',
         'm_pay_type' => 'required',
      ], [
             
      ]);

      if ($request->m_customer_type=="Business") {
      	$this->validate($request, [
	         'organization_id' => 'required',  
	      ], []);
      }else{
      	$this->validate($request, [
	         'm_c_first_name' => 'required',  
	         'm_c_last_name' => 'required', 
	      ], []);
      }

      $org=new MasterJob;

      $org->m_creator_id=auth()->user()->id;

      $org->m_job_id=$request->m_job_id;
      $org->m_name=$request->m_name;
      $org->m_status=$request->m_status;
      $org->m_date_opened=Carbon::createFromFormat('d M Y', $request->m_date_opened)->format('Y-m-d');
      $org->m_lead_technician=$request->m_lead_technician;
      $org->m_source=$request->m_source;

      $org->m_customer_type=$request->m_customer_type;
      if ($request->m_customer_type=="Business") {
      	$org->organization_id=$request->organization_id;
      }elseif ($request->m_customer_type=="Individual") {
      	$org->m_c_first_name=$request->m_c_first_name;
      	$org->m_c_last_name=$request->m_c_last_name;
      }

      $org->m_job_address=$request->m_job_address;
      $org->m_job_apartment=$request->m_job_apartment;
      $org->m_job_city=$request->m_job_city;
      $org->m_job_state=$request->m_job_state;
      $org->m_job_zip=$request->m_job_zip;
      $org->m_job_country='United States';

      $org->m_pay_type=$request->m_pay_type;
      if ($request->m_pay_type=="Insurance") {
      	$org->m_ins_claim=$request->m_ins_claim;
      	$org->m_ins_policy=$request->m_ins_policy;
      	$org->m_ins_company=$request->m_ins_company;
      	$org->m_ins_deductible=$request->m_ins_deductible;
      }

      $org->m_referral=$request->m_referral;
      $org->m_loss_desc=$request->m_loss_desc;

      try {
      	$org->save();
      } catch (Exception $e) {
      	return redirect()->back()->with('error','Oops! Something Went Wrong.');
      }

      return redirect('master-jobs/list')->with('success','Master Job Created!');

	}

	public function edit($mjob_id)
	{
		$orgs=Organization::pluck('org_name','org_id');
		$masterJob=MasterJob::findOrFail($mjob_id);

		return view('backend.core.master.edit',compact('orgs','masterJob'));
	}

	public function update(Request $request,$mjob_id)
	{
		$this->validate($request, [
         'm_job_id' => 'required|unique:master_jobs,m_job_id,'.$mjob_id.',mjob_id',
         'm_name' => 'required', 
         'm_status' => 'required', 
         'm_date_opened' => 'date_format:d M Y|required', 
         'm_customer_type' => 'required|in:'.implode(',', config('master.master_jobs.customer_type')), 

         'm_job_address' => 'required',
         'm_job_city' => 'required',
         'm_job_state' => 'required',
         'm_job_zip' => 'required',
         'm_pay_type' => 'required',
      ], [
             
      ]);

      if ($request->m_customer_type=="Business") {
      	$this->validate($request, [
	         'organization_id' => 'required',  
	      ], []);
      }else{
      	$this->validate($request, [
	         'm_c_first_name' => 'required',  
	         'm_c_last_name' => 'required', 
	      ], []);
      }

		$org=MasterJob::findOrFail($mjob_id);

		$org->m_job_id=$request->m_job_id;
      $org->m_name=$request->m_name;
      $org->m_status=$request->m_status;
      $org->m_date_opened=Carbon::createFromFormat('d M Y', $request->m_date_opened)->format('Y-m-d');
      $org->m_lead_technician=$request->m_lead_technician;
      $org->m_source=$request->m_source;

      $org->m_customer_type=$request->m_customer_type;

      if ($request->m_customer_type=="Business") {
      	$org->organization_id=$request->organization_id;
      	//removing other detials
      	$org->m_c_first_name=null;
      	$org->m_c_last_name=null;
      }elseif ($request->m_customer_type=="Individual") {
      	$org->m_c_first_name=$request->m_c_first_name;
      	$org->m_c_last_name=$request->m_c_last_name;
      	$org->organization_id=null;
      }

      $org->m_job_address=$request->m_job_address;
      $org->m_job_apartment=$request->m_job_apartment;
      $org->m_job_city=$request->m_job_city;
      $org->m_job_state=$request->m_job_state;
      $org->m_job_zip=$request->m_job_zip;
      $org->m_job_country='United States';

      $org->m_pay_type=$request->m_pay_type;
      if ($request->m_pay_type=="Insurance") {
      	$org->m_ins_claim=$request->m_ins_claim;
      	$org->m_ins_policy=$request->m_ins_policy;
      	$org->m_ins_company=$request->m_ins_company;
      	$org->m_ins_deductible=$request->m_ins_deductible;
      }else{
      	$org->m_ins_claim=null;
      	$org->m_ins_policy=null;
      	$org->m_ins_company=null;
      	$org->m_ins_deductible=null;
      }

      $org->m_referral=$request->m_referral;
      $org->m_loss_desc=$request->m_loss_desc;

      try {
      	$org->save();
      } catch (Exception $e) {
      	return redirect()->back()->with('error','Oops! Something Went Wrong.');
      }

      return redirect('master-jobs/list')->with('success','Master Job Updated!');

	}

	public function delete($mjob_id)
	{
		try {
            $org=MasterJob::findOrFail($mjob_id);
            $org->delete();
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return redirect()->back()->with('error','Oops! Something Went Wrong.');
        }

        return redirect()->back()->with('success',$org->m_job_id.' Deleted!');
	}

}
