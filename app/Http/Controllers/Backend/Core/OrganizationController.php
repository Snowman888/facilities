<?php

namespace App\Http\Controllers\Backend\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Organization;
use DataTables;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.core.oraganizations.index');
    }

    public function datatables()
    {
        $orgs=Organization::withCount('masterJobs');

        return Datatables::eloquent($orgs)                        
            ->addColumn('action', function ($org) {
                      return $org->org_id;
                 })
            ->rawColumns(['action'])                   
            ->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.core.oraganizations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOrganizationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'org_name' => 'required|unique:organizations,org_name',       
            'org_email' => 'nullable|email',        
            'org_type' => 'required|in:'.implode(',', config('master.organizations.type')), 
         ], [
                
         ]);

         try {
            Organization::create($request->all());
         } catch (Exception $e) {
            return back()->with('error','Oops! Something Went Wrong.');
         }
        
        return redirect('organizations')->with('success','New Organization Created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit($org_id)
    {
        $org=Organization::findOrFail($org_id);
        return view('backend.core.oraganizations.edit',compact('org'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOrganizationRequest  $request
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $org_id)
    {
        try {
            $org=Organization::findOrFail($org_id);
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return redirect()->back()->with('error','Oops! Something Went Wrong.');
        }

        $this->validate($request, [
            'org_name' => 'required|unique:organizations,org_name,'.$org_id.',org_id',       
            'org_email' => 'nullable|email',        
            'org_type' => 'required|in:'.implode(',', config('master.organizations.type')), 
         ], [
                
         ]);

        

        $flight = $org->updateOrCreate(
            ['org_id' => $org->org_id],
            $request->all()            
        );

        return redirect('organizations')->with('success',$org->org_name.' Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy($org_id)
    {
        try {
            $org=Organization::findOrFail($org_id);
            $org->delete();
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return redirect()->back()->with('error','Oops! Something Went Wrong.');
        }

        return redirect('organizations')->with('success',$org->org_name.' Deleted!');
    }
}
