<?php

namespace App\Http\Controllers\Backend\Core;

use App\Http\Controllers\Controller;
use App\Models\MasterJob;
use App\Models\ProjectFile;
use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon;

class ProjectFilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($mjob_id)
    {
        $masterJob=MasterJob::findOrFail($mjob_id);

        return view('backend.core.projectFiles.index',compact('masterJob'));
    }

    public function datatables($mjob_id)
    {
        $projects=ProjectFile::where('project_mjob_id',$mjob_id);

        return Datatables::eloquent($projects)                       
            ->addColumn('action', function ($project) {
                      return $project->project_id;
                 })
            ->editColumn('m_date_opened', function($mJob) {
              return Carbon::createFromFormat('Y-m-d', $mJob->m_date_opened)->format('d M Y');
          })
            ->rawColumns(['action'])                   
            ->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\MasterJob  $masterJob
     * @return \Illuminate\Http\Response
     */
    public function create(MasterJob $masterJob)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterJob  $masterJob
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, MasterJob $masterJob)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterJob  $masterJob
     * @param  \{{ namespacedModel }}  ${{ modelVariable }}
     * @return \Illuminate\Http\Response
     */
    public function show(MasterJob $masterJob, $mjob_id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterJob  $masterJob
     * @param  \{{ namespacedModel }}  ${{ modelVariable }}
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterJob $masterJob,$mjob_id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterJob  $masterJob
     * @param  \{{ namespacedModel }}  ${{ modelVariable }}
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterJob $masterJob, $mjob_id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterJob  $masterJob
     * @param  \{{ namespacedModel }}  ${{ modelVariable }}
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterJob $masterJob, $mjob_id)
    {
        //
    }
}
