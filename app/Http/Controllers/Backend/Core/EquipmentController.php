<?php

namespace App\Http\Controllers\Backend\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Equipment;
use DataTables;
use Carbon\Carbon;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.core.equipments.index');
    }

    public function datatables()
    {
        $equips=Equipment::query();

        return Datatables::eloquent($equips)                       
                ->addColumn('action', function ($equip) {
                    return $equip->equip_id;
                })
                ->editColumn('equip_p_date', function($equip) {
                    if ($equip->equip_p_date) {
                        return Carbon::createFromFormat('Y-m-d', $equip->equip_p_date)->format('m/d/Y');
                    }else{return null;}
                    
                })
        ->rawColumns(['action'])                   
        ->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.core.equipments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
         'equip_e_id' => 'required',  
         'equip_status' => 'required|in:'.implode(',', config('master.equipments.status')), 

         'equip_p_date' => 'date_format:m/d/Y|nullable', 
         'equip_p_cost' => 'nullable|numeric|regex:/^[0-9]*(\.[0-9]{0,2})?$/',
      ], [
            'equip_p_date.date_format' => 'Invalid Date',      
      ]);

        if ($request->equip_p_date) {
            $request->merge([
                'equip_p_date' => Carbon::createFromFormat('m/d/Y', $request->equip_p_date)->format('Y-m-d'),
            ]);
        }
        

        try {
            Equipment::create($request->all());
         } catch (Exception $e) {
            return back()->with('error','Oops! Something Went Wrong.');
         }
        
        return redirect('equipments')->with('success','New Equipment Added!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equip=Equipment::findOrFail($id);

        return view('backend.core.equipments.edit',compact('equip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $equip=Equipment::findOrFail($id);
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return redirect()->back()->with('error','Oops! Something Went Wrong.');
        }

        $this->validate($request, [
         'equip_e_id' => 'required',  
         'equip_status' => 'required|in:'.implode(',', config('master.equipments.status')), 

         'equip_p_date' => 'date_format:m/d/Y|nullable', 
         'equip_p_cost' => 'nullable|numeric|regex:/^[0-9]*(\.[0-9]{0,2})?$/',
      ], [
            'equip_p_date.date_format' => 'Invalid Date',      
      ]);

        if ($request->equip_p_date) {
            $request->merge([
                'equip_p_date' => Carbon::createFromFormat('m/d/Y', $request->equip_p_date)->format('Y-m-d'),
            ]);
        }
        

        try {
            $equip->updateOrCreate(
                ['equip_id' => $id],
                $request->all()            
            );
         } catch (Exception $e) {
            return back()->with('error','Oops! Something Went Wrong.');
         }
        
        return redirect('equipments')->with('success','Equipment Detail Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $org=Equipment::findOrFail($id);
            $org->delete();
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return redirect()->back()->with('error','Oops! Something Went Wrong.');
        }

        return redirect()->back()->with('success',$org->equip_e_id.' Deleted!');
    }
}
