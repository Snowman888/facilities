<?php
namespace App\Helpers; 
use App\User;
use Auth;
class UserRoleAuthorize
{
	public static function accessMenu($item): ? bool
	{
		if (!array_key_exists("roles",$item)) {
			return true;
		}

		if (auth()->user()->hasRole($item['roles'])) {
			return true;
		}

		return false;
	}
	
}