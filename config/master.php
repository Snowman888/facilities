<?php

return [
   'master_jobs' => [
     	'status' => [
         'New' => 'New',
         'Production' => 'Production',
         'Closed' => 'Closed',
         'Billed' => 'Billed',
   	],
     	'customer_type' => [
         'Business' => 'Business',
         'Individual' => 'Individual',
   	],
      'pay_method' => [
         'Insurance' => 'Insurance',
         'Self-Pay' => 'Self-Pay',
         'Other' => 'Other',
      ]
   ],
   'organizations' => [
     	'type' => [
         'Customer' => 'Customer',
         'Insurance Co' => 'Insurance Co',
         'Other' => 'Other',
   	]
   ],
   'contacts' => [
      'type' => [
         'Customer - Primary' => 'Customer - Primary',
         'Customer - Secondary' => 'Customer - Secondary',
         'Insurance Agent' => 'Insurance Agent',
         'Insurance Adjuster' => 'Insurance Adjuster',
         'Inspector' => 'Inspector',
         'IAQ' => 'IAQ',
      ]
   ],
   'equipments' => [
      'status' => [
         'Available' => 'Available',
         'On Job' => 'On Job',
         'To be Cleaned' => 'To be Cleaned',
         'Retired' => 'Retired',
         'Discarded' => 'Discarded',
      ]
   ],
   
];
