<?php
return [

	'menu' => [
        // Navbar items:
        [
            'type'         => 'navbar-search',
            'text'         => 'search',
            'topnav_right' => true,
        ],
        [
            'type'         => 'fullscreen-widget',
            'topnav_right' => true,
        ],

        // Sidebar items:
        [
            'type' => 'sidebar-menu-search',
            'text' => 'search',
            'class' => 'btn-sm',
        ],
        [
            'text'        => 'Dashboard',
            'route'       => 'dashboard',
            'icon'        => 'nav-icon fas fa-tachometer-alt',
        ],
        [
            'text'        => 'Master Jobs',
            'url'         => '/master-jobs/list',
            'icon'        => 'nav-icon fas fa-briefcase',
            'active'      => ['master-jobs/*'],
        ],
        [
            'text'        => 'Equipments',
            'url'         => '/equipments',
            'icon'        => 'nav-icon fas fa-tools',
            'active'      => ['equipments/*'],
        ],
        [
            'text'        => 'Organizations',
            'url'         => '/organizations',
            'icon'        => 'nav-icon fas fa-sitemap',
            'active'      => ['organizations/*'],
        ],
        [
            'text'        => 'Contacts',
            'url'         => '/contacts',
            'icon'        => 'nav-icon fas fa-address-book',
            'active'      => ['contacts/*'],
        ],

        //Settings
        ['header' => 'Settings'],
        [
            'text' => 'profile',
            'url'  => 'settings/profile',
            'icon' => 'fas fa-fw fa-user',
        ],
        [
            'text'    => 'Users',
            'icon'    => 'fas fa-fw fa-users',
            'active' => ['settings/users/edit/*'],
            'roles' =>['Super Admin','Admin'],
            'submenu' => [
                [
                    'text' => 'List',
                    'url'  => 'settings/users',
                ],
                [
                    'text' => 'Create',
                    'url'  => 'settings/users/create',
                ],
            ],
        ],
       
        ['header' => 'labels'],
        [
            'text'       => 'important',
            'icon_color' => 'red',
            'url'        => '#',
        ],
        [
            'text'       => 'warning',
            'icon_color' => 'yellow',
            'url'        => '#',
        ],
        [
            'text'       => 'information',
            'icon_color' => 'cyan',
            'url'        => '#',
        ],
    ],

    ];

