
function dangerNotification(title,text,icon,confirmButtonText,form,confirmButtonColor='#001f3f',cancelButtonColor='#d33',iconColor='#dc3545')
{
	new swal({
		title: title,
		text: text,
		icon: icon,
		showCancelButton: true,
		confirmButtonColor: confirmButtonColor,
		cancelButtonColor: cancelButtonColor,
		confirmButtonText: confirmButtonText,
		iconColor:iconColor,
	}).then((result) => {
		if (result.isConfirmed) {
			swal(form.submit(), {});

		} else if (result.isDismissed) {
		    // A dialog has been cancelled
		    // For more information about handling dismissals please visit
		    // https://sweetalert2.github.io/#handling-buttons
		 }
		})
}