$.extend( true, $.fn.dataTable.defaults, {
     initComplete : function() {
        var input = $('.dataTables_filter input').unbind(),
            self = this.api(),
            $space = $('<span>')
                     .text(' '),                   
            $searchButton = $('<button>')
                     .addClass("btn btn-custom btn-sm")
                     .css('margin-top', '-2px')
                     .text('Search')
                     .click(function() {
                          self.search(input.val()).draw();
                     })
        $('.dataTables_filter').append($space,$searchButton);
    }      
} );
