//IMAGE VALIDATION for parsely
var app = app || {};
// Utils
(function ($, app) {
    'use strict';

    app.utils = {};

    app.utils.formDataSuppoerted = (function () {
        return !!('FormData' in window);
    }());

        }(jQuery, app));

        // Parsley validators
        (function ($, app) {
            'use strict';

            window.Parsley
                .addValidator('filemaxmegabytes', {
                    requirementType: 'string',
                    validateString: function (value, requirement, parsleyInstance) {

                        if (!app.utils.formDataSuppoerted) {
                            return true;
                        }

                        var file = parsleyInstance.$element[0].files;
                        var maxBytes = requirement * 1048576;

                        if (file.length == 0) {
                            return true;
                        }

                        return file.length === 1 && file[0].size <= maxBytes;

                    },
                    messages: {
                        en: 'File is to big. Logo must be less than 2MB'
                    }
                })
                .addValidator('filemimetypes', {
                    requirementType: 'string',
                    validateString: function (value, requirement, parsleyInstance) {

                        if (!app.utils.formDataSuppoerted) {
                            return true;
                        }

                        var file = parsleyInstance.$element[0].files;

                        if (file.length == 0) {
                            return true;
                        }

                        var allowedMimeTypes = requirement.replace(/\s/g, "").split(',');
                        return allowedMimeTypes.indexOf(file[0].type) !== -1;

                    },
                    messages: {
                        en: 'This is not an Image, Only Jpg, Jpeg,bmp,png are allowed'
                    }
                });

}(jQuery, app));