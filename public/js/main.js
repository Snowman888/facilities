var button=null;
	var submit=null;
	var form=null;

	
	$('form').submit(function(event) {
		button = $(":button[type=submit]",this);
		submit = $("input[type=submit]",this);
		form = $("form",this);
		$(button).attr("disabled", true);
		$(submit).attr("disabled", true);
	
	});
	$(':input').click(function() 
	{
	    $(button).removeAttr("disabled");
	    $(submit).removeAttr("disabled");
	});
	$(":input").keyup(function (event)
	{
	    $(button).removeAttr("disabled");
	    $(submit).removeAttr("disabled");
	});
	
$(document).ready(function () {
    $('form').attr('autocomplete', 'chrome-off');
    $('form').attr('role', 'presentation');

    $('input').attr('autocorrect', 'off');
    $('input').attr('autocomplete', 'chrome-off');
    $('input').attr('autocapitalize', 'off');

    $('.fake').attr('autocorrect', 'on');
    $('.fake').attr('autocomplete', 'on');
});


function changeErrorState() {
	$(button).removeAttr("disabled");
	$(submit).removeAttr("disabled");
	$( "#searchErrorID" ).remove(); 
}

//custom file label to change the file name on file input box