@if(Session::has('success'))

	<script type="text/javascript">
		Swal.fire({
		   toast: true,
		   
		 	showCloseButton: true,
		   icon: 'success',
		   title: "{!! Session::get('success') !!}",
		   animation: false,
		   position: 'top-right',
		   showConfirmButton: false,
		   timer: 3000
		   ,
		   timerProgressBar: true,
		   didOpen: (toast) => {
		      toast.addEventListener('mouseenter', Swal.stopTimer)
		      toast.addEventListener('mouseleave', Swal.resumeTimer)
		   }
		})
	</script>
@endif


{{-- error message --}}
@if(count($errors->all()) > 0 || Session::has('error'))
	<script type="text/javascript">
		Swal.fire({
		   toast: true,	
		   icon: 'error',
		   title: "Opps, There Was An Error!!",
		   animation: false,
		   position: 'top-right',
		   showConfirmButton: false,
		   showCloseButton: true,
		   timer: 3000
		   ,
		   timerProgressBar: true,
		   didOpen: (toast) => {
		      toast.addEventListener('mouseenter', Swal.stopTimer)
		      toast.addEventListener('mouseleave', Swal.resumeTimer)
		   }
		})
	</script>
@endif