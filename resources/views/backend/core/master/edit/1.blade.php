<div class="card card-navy  card-outline">
   <div class="card-header">
      <h4 class="card-title">Job Details</h4>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
    
      <div class="row">
         <div class="col-sm-6">
          {{-- Job ID --}}
            <div class="form-group {{ $errors->has('m_job_id') ? 'has-error' : ''}}">
               <label for="m_job_id">Job ID</label>
               <div class="error-block">
                  {!!Form::text('m_job_id',                      
                     $masterJob->m_job_id,
                     array(
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'Job Id Is Required',
                        'data-parsley-group'=>'step-member',             
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_job_id'),
                        'placeholder'=>'- -',
                        'id'=>'m_job_id',
                        'class'=>'form-control',                        
                     )
                  )!!}                        
                  @foreach ($errors->get('m_job_id') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>

         {{-- Job name --}}
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('m_name') ? 'has-error' : ''}}">
               <label for="m_name">Job Name</label>
               <div class="error-block">
                  {!!Form::text('m_name',                      
                     $masterJob->m_name,
                     array(
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'Job Name Is Required',
                        'data-parsley-group'=>'step-member',             
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_name'),
                        'placeholder'=>'- -',
                        'id'=>'m_name',
                        'class'=>'form-control',
                     )
                  )!!}                        
                  @foreach ($errors->get('m_name') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
      </div>

      <div class="row">
         {{-- Job status --}}
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('m_status') ? 'has-error' : ''}}">
               <label for="m_status">Job Status</label>
               <div class="error-block">
                  {!!Form::select('m_status',
                     config('master.master_jobs.status'), 
                     $masterJob->m_status,
                     array(
                        'data-parsley-group'=>'step-member',   
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'Status Is Required',
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_status'),
                        'class'=>'form-control',
                        'data-size'=>'10',
                        'id'=>'client',
                        'data-live-search'=>'false',
                        'data-style'=>'btn-white',
                        'tabindex'=>'-98',
                        'placeholder'=>'--Select Status--'
                     )
                  )!!}                       
                  @foreach ($errors->get('m_status') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>

         {{-- Date Opened --}}
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('m_date_opened') ? 'has-error' : ''}}">
               <label for="m_date_opened">Date Opened</label>
               <div class="error-block">
                  {!!Form::text('m_date_opened',
                     \Carbon::createFromFormat('Y-m-d', $masterJob->m_date_opened)->format('d M Y'), 
                     array(
                        'data-parsley-group'=>'step-member',  
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'Date Opened Is Required',
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => date('DD MMM YYYY', strtotime(Input::old('event_date'))),
                        'placeholder'=>'- -',
                        'class'=>'form-control datetimepicker-input',
                        'data-date-format'=>'DD MMM YYYY',
                        'id'=>'m_date_opened',
                        'data-parsley-no-focus',
                        'data-toggle'=>"datetimepicker"
                     )
                  )!!}                       
                  @foreach ($errors->get('m_date_opened') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
      </div> 

      <div class="row">
         {{-- Lead Technician --}}
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('m_lead_technician') ? 'has-error' : ''}}">
               <label for="m_lead_technician">Lead Technician</label>
               <div class="error-block">
                  {!!Form::text('m_lead_technician',                      
                     $masterJob->m_lead_technician,
                     array(                       
                        'data-parsley-group'=>'step-member',             
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_lead_technician'),
                        'placeholder'=>'- -',
                        'id'=>'m_lead_technician',
                        'class'=>'form-control',
                     )
                  )!!}                       
                  @foreach ($errors->get('m_lead_technician') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>

         {{-- Source --}}
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('m_source') ? 'has-error' : ''}}">
               <label for="m_source">Source</label>
               <div class="error-block">
                  {!!Form::text('m_source',
                     $masterJob->m_source, 
                     array(                       
                       
                        'value' => Input::old('m_source'),
                        'placeholder'=>'- -',
                        'class'=>'form-control',
                       
                        'id'=>'m_source',                        
                     )
                  )!!}                       
                  @foreach ($errors->get('m_source') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
      </div> 
        
   </div>
  <!-- /.card-body -->
</div>