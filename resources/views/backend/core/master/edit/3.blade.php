<div class="card card-navy  card-outline">
   <div class="card-header">
      <h4 class="card-title">Job Location</h4>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
    
      {{-- first Row --}}
      <div class="row">
         <div class="col">
          {{-- Address --}}
            <div class="form-group {{ $errors->has('m_job_address') ? 'has-error' : ''}}">
               <label for="m_job_address">Address</label>
               <div class="error-block">
                  {!!Form::text('m_job_address',                      
                     $masterJob->m_job_address,
                     array(
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'Address Is Required',
                        'data-parsley-group'=>'step-member',             
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_job_address'),
                        'placeholder'=>'- -',
                        'id'=>'m_job_address',
                        'class'=>'form-control',
                     )
                  )!!}                            
                  @foreach ($errors->get('m_job_address') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>        
      </div> 

      {{-- Second Row --}}
      <div class="row">
         <div class="col-sm-3">
          {{-- Apartment --}}
            <div class="form-group {{ $errors->has('m_job_apartment') ? 'has-error' : ''}}">
               <label for="m_job_apartment">Apartment, unit, suite, or floor #</label>
               <div class="error-block">
                  {!!Form::text('m_job_apartment',                      
                     $masterJob->m_job_apartment,
                     array(                      
                        'value' => Input::old('m_job_apartment'),
                        'placeholder'=>'- -',
                        'id'=>'m_job_apartment',
                        'class'=>'form-control',
                     )
                  )!!}                        
                  @foreach ($errors->get('m_job_apartment') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
         {{-- City --}}
         <div class="col-sm-3">
            <div class="form-group {{ $errors->has('m_job_city') ? 'has-error' : ''}}">
               <label for="m_job_city">City</label>
               <div class="error-block">
                  {!!Form::text('m_job_city',                      
                     $masterJob->m_job_city,
                     array(
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'City Is Required',
                        'data-parsley-group'=>'step-member',             
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_job_city'),
                        'placeholder'=>'- -',
                        'id'=>'m_job_city',
                        'class'=>'form-control',
                     )
                  )!!}                        
                  @foreach ($errors->get('m_job_city') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
         {{-- State --}}
         <div class="col-sm-3">
            <div class="form-group {{ $errors->has('m_job_state') ? 'has-error' : ''}}">
               <label for="m_job_state">State</label>
               <div class="error-block">
                  {!!Form::text('m_job_state',                      
                     $masterJob->m_job_state,
                     array(
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'State Is Required',
                        'data-parsley-group'=>'step-member',             
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_job_state'),
                        'placeholder'=>'- -',
                        'id'=>'m_job_state',
                        'class'=>'form-control',
                     )
                  )!!}                        
                  @foreach ($errors->get('m_job_state') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
         <div class="col-sm-3">
          {{-- Zip --}}
            <div class="form-group {{ $errors->has('m_job_zip') ? 'has-error' : ''}}">
               <label for="m_job_zip">Zip</label>
               <div class="error-block">
                  {!!Form::text('m_job_zip',                      
                     $masterJob->m_job_zip,
                     array(
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'Zip Is Required',
                        'data-parsley-group'=>'step-member',             
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_job_zip'),
                        'placeholder'=>'- -',
                        'id'=>'m_job_zip',
                        'class'=>'form-control',
                     )
                  )!!}                        
                  @foreach ($errors->get('m_job_zip') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </div>
  <!-- /.card-body -->
</div>



@push('js')
  


@endpush