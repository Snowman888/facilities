<div class="card card-navy  card-outline">
   <div class="card-header">
      <h4 class="card-title">Referral</h4>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
    
      {{-- first Row --}}
      <div class="row">
         <div class="col">
          {{-- Address --}}
            <div class="form-group {{ $errors->has('m_referral') ? 'has-error' : ''}}">
               <label for="m_referral">Referral</label>
               <div class="error-block">
                  {!!Form::text('m_referral',
                     $masterJob->m_referral, 
                     array(
                        'value' => Input::old('m_referral'),
                        'placeholder'=>'- -',
                        'class'=>'form-control',
                       
                        'id'=>'m_referral',                        
                     )
                  )!!}                               
                  @foreach ($errors->get('m_referral') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>        
      </div> 
  <!-- /.card-body -->
   </div>
</div>



@push('js')

@endpush