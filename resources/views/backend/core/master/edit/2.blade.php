<div class="card card-navy  card-outline">
   <div class="card-header">
      <h4 class="card-title">Customer Details</h4>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
    
      <div class="row">
         <div class="col">
          {{-- Job ID --}}
            <div class="form-group {{ $errors->has('m_customer_type') ? 'has-error' : ''}}">
               <label for="m_customer_type">Customer Type</label>
               <div class="error-block">
                  {!!Form::select('m_customer_type',
                     config('master.master_jobs.customer_type'), 
                     $masterJob->m_customer_type,
                     array(
                        'data-parsley-group'=>'step-member',
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'Customer Type Is Required',
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_customer_type'),
                        'class'=>'form-control',
                        'data-size'=>'10',
                        'id'=>'m_customer_type',
                        'data-live-search'=>'false',
                        'data-style'=>'btn-white',
                        'tabindex'=>'-98',
                        'placeholder'=>'--Select Customer Type--'
                     )
                  )!!}                         
                  @foreach ($errors->get('m_customer_type') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>        
      </div> 


      {{-- Customer Individual --}}
      <div class="row" style="{{$masterJob->m_customer_type=='Individual'?'':'display: none'}}" id="Individual_Customer">
         <div class="col-sm-6">
          {{-- Customer First Name --}}
            <div class="form-group {{ $errors->has('m_c_first_name') ? 'has-error' : ''}}">
               <label for="m_c_first_name">Customer First Name</label>
               <div class="error-block">
                  {!!Form::text('m_c_first_name',                      
                     $masterJob->m_c_first_name,
                     array(
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'First Name Is Required',
                        'data-parsley-group'=>'step-member',             
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_c_first_name'),
                        'placeholder'=>'- -',
                        'id'=>'m_c_first_name',
                        'class'=>'form-control',
                     )
                  )!!}                        
                  @foreach ($errors->get('m_c_first_name') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>

         {{-- Customer Last Name  --}}
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('m_c_last_name') ? 'has-error' : ''}}">
               <label for="m_c_last_name">Customer Last Name</label>
               <div class="error-block">
                  {!!Form::text('m_c_last_name',                      
                     $masterJob->m_c_last_name,
                     array(
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'Last Name Is Required',
                        'data-parsley-group'=>'step-member',             
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_c_last_name'),
                        'placeholder'=>'- -',
                        'id'=>'m_c_last_name',
                        'class'=>'form-control',
                     )
                  )!!}                        
                  @foreach ($errors->get('m_c_last_name') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
      </div>

      {{-- Customer Business --}}
      <div class="row" style="{{$masterJob->m_customer_type=='Business'?'':'display: none'}}" id="Business_Customer">
         <div class="col">
          {{-- Customer First Name --}}
            <div class="form-group {{ $errors->has('organization_id') ? 'has-error' : ''}}">
               <label for="organization_id">Business Name</label>
               <div class="error-block">
                  {!!Form::select('organization_id',
                     $orgs, 
                     $masterJob->organization_id,
                     array(
                        'data-parsley-group'=>'step-member', 
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'Business Name Is Required',
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('organization_id'),
                        'class'=>'form-control organization_id',
                        'data-size'=>'10',
                        'id'=>'organization_id',    
                        'placeholder'=>'- -',                 
                     )
                  )!!}                           
                  @foreach ($errors->get('organization_id') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </div>
  <!-- /.card-body -->
</div>

@push('js')
   <script type="text/javascript">
      //changing the customer type
      $('#m_customer_type').on('change', function(event) {
         if (this.value=="Individual") {
            $( "#Business_Customer" ).hide( "fast" );
            $( "#Individual_Customer" ).show( "fast" );         
         } else{
            $( "#Individual_Customer" ).hide( "fast" );
            $( "#Business_Customer" ).show( "fast" );         
         };    
         event.preventDefault();       
      });
   </script>

   <script type="text/javascript">
      $(document).ready(function() {
          $('.organization_id').select2({
            placeholder: "- -",
            allowClear: true
          });
      });
   </script>
@endpush