@extends('adminlte::page')

@section('title_prefix', 'Master Jobs - ')

@section('content_header')
    <h1>Master Jobs</h1>
@stop

@section('content')
   <div class="card">
     <!-- /.card-header -->
     <div class="card-body">
       <div class="float-right">
         <a href="{{url('master-jobs/create')}}" class="btn btn-orange"><i class="fas fa-plus"></i> Add Master Job</a>
      </div>
     </div>
     <!-- /.card-body -->
   </div>

   <div class="card card-navy">
      <div class="card-header">
         <h3 class="card-title">Master Jobs</h3>

         <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
               <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
               <i class="fas fa-times"></i>
            </button>
         </div>
      </div>
      
   <div class="card-body">
      <table id="mJobs" class="table table-bordered table-striped display" nowrap width="100%">
         <thead>
            <tr>        
               <th style="white-space: nowrap;"></th> 
               <th style="white-space: nowrap;" data-priority="2">Job ID</th>
               <th style="white-space: nowrap;">Name</th>
               <th style="white-space: nowrap;">Status</th>                    
               <th style="white-space: nowrap;">Date Opened</th>       
               <th style="white-space: nowrap;text-align: center;" class="no-sort notexport text-nowrap" data-priority="1" style="width:81px"></th>                    
            </tr>
         </thead>
         <tbody>                         
         </tbody>
         <tfoot>
            <tr>
               <th></th>
               <th>Job ID</th>  
               <th>Name</th>           
               <th>Status</th>               
               <th>Date Opened</th>
               <th data-priority="1"></th>    
            </tr>
         </tfoot>
      </table>      
   </div>
   <!-- /.card-body -->

   <!-- /.card-footer-->
   </div>
@stop

@section('footer')
  {{--  <span>
      <button class="btn btn-custom">Create Job</button>
   </span> --}}
@stop

@section('css')
   <link rel="stylesheet" href="{{asset('vendor/datatables/css/dataTables.bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/datatables-plugins/responsive/css/responsive.bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/datatables-plugins/buttons/css/buttons.bootstrap4.min.css')}}">
@stop

@section('js')
   <script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
   <script src="{{asset('vendor/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js')}}"></script>

   <script src="{{asset('vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js')}}"></script>

   <script src="{{asset('js/datatables-extentions.js')}}"></script>
   <script type="text/javascript">
      $(function () {
         $('#mJobs').DataTable({
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "fixedHeader": true,        
            'paging'      : true,
            'pageLength'  : 10,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'scrollX'     : false,
            'autoWidth'   : true, 
            ajax: {
               url: "{!! url('master-jobs/list-datatables') !!}",
            },
            "columns":[
               { "data": "mjob_id" }, 
               { "data": "m_job_id", "name": "m_job_id",
                  fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                     $(nTd).html("<a href='{!!url('master-jobs/"+oData.mjob_id+"/project_files')!!}'>"+oData.m_job_id+"</a>");
                  }
               },             
               // { "data": "m_job_id" },                
               { "data": "m_name" }, 
               { "data": "m_status" }, 
               { "data": "m_date_opened" },              
               { data: 'action', name: 'action', orderable: false, searchable: false,
                   render: function (data) { 
                     return '<form action="{!!url("master-jobs/delete/'+data+'")!!}" method="POST" enctype="multipart/form-data" >'+
                           '{{csrf_field()}} @method("DELETE")'+
                           '<span style="display: inline;white-space: nowrap;">'+
                           '<a href="{!!url("master-jobs/edit/'+data+'")!!}" class="btn btn-xs btn-custom"><i class="fas fa-fw fa-edit"></i></a> &nbsp;&nbsp;'+
                           '<a href="javascript:;" class="btn btn-danger btn-xs" data-click="swal-danger" ><i class="fa fa-trash-alt"></i></a>&nbsp;&nbsp;'+
                           '<a href="{!!url("master-jobs/'+data+'/project_files")!!}" class="btn btn-success btn-xs"><i class="fa fa-file"></i></a>'+
                           '</span>'+
                        '</form>';}
               }, 
            ],
            'order':[[0,'desc']],
            'columnDefs':[
               {orderable: false,targets: "no-sort"},
               {"targets": [0],visible: false},
               { width: "10%", targets: 5 },
               {"targets": [5],"className": "text-center nowrap"},
            ],
         })
      })
   </script>
   <script type="text/javascript">
      $("#mJobs").on("click", "[data-click='swal-danger']", function (e) { 
         var form = $(this).parents('form');
         dangerNotification('Are You Sure?','You are about to delete this master job.','question','Delete',form)
      });
   </script>

   <script type="text/javascript">
      $('#mJobs').on('click', 'tbody td:first-child', function() {

        console.log('asdsda');
      })
   </script>
@stop
