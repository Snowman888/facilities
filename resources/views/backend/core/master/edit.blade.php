@extends('adminlte::page')

@section('title_prefix', 'Edit Master Job - ')

@section('content_header')
    <h1>Edit Master Job</h1>
@stop

@section('content')
   
   <form class="form-horizontal" method="POST" name="form" id="myForm" enctype="multipart/form-data">
   @csrf
      @include('backend.core.master.edit.1')
      @include('backend.core.master.edit.2')
      @include('backend.core.master.edit.3')
      @include('backend.core.master.edit.4')
      @include('backend.core.master.edit.5')
      @include('backend.core.master.edit.6')

      <br/><br/>

   <footer class="main-footer">
          <div class="float-right">
         <button type="submit" class="btn btn-custom" id="submit">Update</button>
      </div>
   </footer>

   </form>

@stop



@section('css')
   <link href="{{asset('vendor/bootstrap4-datepicker/css/datepicker.min.css')}}" rel="stylesheet">
   <link href="{{asset('vendor/select2/css/select2.min.css')}}" rel="stylesheet">
   <link href="{{asset('vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}" rel="stylesheet">
   <style type="text/css">
      label {
          color: #828282;
      }
   </style>
@stop

@section('js')
   <script src="{{asset('vendor/moment/moment.min.js')}}"></script>
   <script src="{{asset('vendor/parsley/dist/parsley.js')}}"></script>
   <script src="{{asset('vendor/bootstrap4-datepicker/js/datepicker.min.js')}}"></script>
   <script src="{{asset('vendor/select2/js/select2.min.js')}}"></script>

   <script src="{{asset('js/parsely-extention.js')}}"></script>

   <script type="text/javascript">
      $('#submit').on('click', function(e, anchorObject, stepNumber, stepDirection) {
            var res = $('form[name="form"]').parsley().validate('step-member');
            return res;
        });
   </script>

   <script type="text/javascript">
      $('.datetimepicker-input').datetimepicker();
   </script>
@stop
