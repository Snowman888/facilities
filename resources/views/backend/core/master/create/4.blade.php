<div class="card card-navy  card-outline">
   <div class="card-header">
      <h4 class="card-title">Payment Method</h4>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
    
      {{-- first Row --}}
      <div class="row">
         <div class="col">
          {{-- Address --}}
            <div class="form-group {{ $errors->has('m_pay_type') ? 'has-error' : ''}}">
               <label for="m_pay_type">Pay Type</label>
               <div class="error-block">
                  {!!Form::select('m_pay_type',
                     config('master.master_jobs.pay_method'), 
                     null,
                     array(
                        'data-parsley-group'=>'step-member', 
                        'data-parsley-required'=>'true',
                        'data-parsley-required-message'=>'Pay Type Is Required',
                        'data-parsley-trigger'=> 'change focusout',
                        'value' => Input::old('m_pay_type'),
                        'class'=>'form-control',
                        'data-size'=>'10',
                        'id'=>'m_pay_type',
                        'data-live-search'=>'false',
                        'data-style'=>'btn-white',
                        'tabindex'=>'-98',
                        'placeholder'=>'--Select Pay Type--'
                     )
                  )!!}                                
                  @foreach ($errors->get('m_pay_type') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>        
      </div> 


      {{-- Insurance --}}
      <div class="row" style="display: none" id="Insurance_field">
         <div class="col-sm-3">
          {{-- Insurance-Claim --}}
            <div class="form-group {{ $errors->has('m_ins_claim') ? 'has-error' : ''}}">
               <label for="m_ins_claim">Insurance-Claim</label>
               <div class="error-block">
                  {!!Form::text('m_ins_claim',                      
                     null,
                     array(  
                        'value' => Input::old('m_ins_claim'),
                        'placeholder'=>'- -',
                        'id'=>'m_ins_claim',
                        'class'=>'form-control',
                     )
                  )!!}                        
                  @foreach ($errors->get('m_ins_claim') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
         {{-- Insurance- Policy --}}
         <div class="col-sm-3">
            <div class="form-group {{ $errors->has('m_ins_policy') ? 'has-error' : ''}}">
               <label for="m_ins_policy">Insurance- Policy</label>
               <div class="error-block">
                  {!!Form::text('m_ins_policy',                      
                     null,
                     array(
                        'value' => Input::old('m_ins_policy'),
                        'placeholder'=>'- -',
                        'id'=>'m_ins_policy',
                        'class'=>'form-control',
                     )
                  )!!}                        
                  @foreach ($errors->get('m_ins_policy') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
         {{-- Insurance- Company --}}
         <div class="col-sm-3">
            <div class="form-group {{ $errors->has('m_ins_company') ? 'has-error' : ''}}">
               <label for="m_ins_company">Insurance- Company</label>
               <div class="error-block">
                  {!!Form::text('m_ins_company',                      
                     null,
                     array(
                        'value' => Input::old('m_ins_company'),
                        'placeholder'=>'- -',
                        'id'=>'m_ins_company',
                        'class'=>'form-control',
                     )
                  )!!}                        
                  @foreach ($errors->get('m_ins_company') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
         <div class="col-sm-3">
          {{-- Insurance- Deductible --}}
            <div class="form-group {{ $errors->has('m_ins_deductible') ? 'has-error' : ''}}">
               <label for="m_ins_deductible">Insurance- Deductible</label>
               <div class="error-block">
                  {!!Form::text('m_ins_deductible',                      
                     null,
                     array(
                        'value' => Input::old('m_ins_deductible'),
                        'placeholder'=>'- -',
                        'id'=>'m_ins_deductible',
                        'class'=>'form-control',
                     )
                  )!!}                        
                  @foreach ($errors->get('m_ins_deductible') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
  <!-- /.card-body -->
   </div>
</div>



@push('js')
   <script type="text/javascript">
      //changing the customer type
      $('#m_pay_type').on('change', function(event) {
         if (this.value=="Insurance") {
            $( "#Insurance_field" ).show( "fast" );         
         } else{
            $( "#Insurance_field" ).hide( "fast" );       
         };    
         event.preventDefault();       
      });
   </script>


@endpush