<div class="card card-navy  card-outline">
   <div class="card-header">
      <h4 class="card-title">Loss Description</h4>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
    
      {{-- first Row --}}
      <div class="row">
         <div class="col">
          {{-- Address --}}
            <div class="form-group {{ $errors->has('m_loss_desc') ? 'has-error' : ''}}">
               <label for="m_loss_desc">Loss Description</label>
               <div class="error-block">
                  {!!Form::textarea('m_loss_desc',
                     null, 
                     array(
                        'value' => Input::old('m_loss_desc'),
                        'placeholder'=>'- -',
                        'class'=>'form-control',
                        'rows'=>'3',
                        'id'=>'m_loss_desc',                        
                     )
                  )!!}                               
                  @foreach ($errors->get('m_loss_desc') as $message) 
                     <p class="help-block">{{$message}}</p>
                  @endforeach
               </div>
            </div>
         </div>        
      </div> 
  <!-- /.card-body -->
   </div>
</div>



@push('js')

@endpush