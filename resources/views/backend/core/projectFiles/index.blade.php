@extends('adminlte::page')

@section('title_prefix', 'Project Files - ')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Project Files</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('master-jobs/list')}}">Master Jobs</a></li>
                    <li class="breadcrumb-item active">Project Files</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="float-right">
                <a href="{{url('contacts/create')}}" class="btn btn-orange"><i class="fas fa-plus"></i> Add Project File</a>
            </div>
        </div>
    </div>
    
    <div class="card card-navy">
        <div class="card-header">
            <h3 class="card-title">Project Files</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <table id="projects" class="table table-bordered table-striped display" nowrap width="100%">
                <thead>
                    <tr>         
                        <th style="white-space: nowrap;"></th>
                        <th style="white-space: nowrap;">ID</th>
                        <th style="white-space: nowrap;">Status</th> 
                        <th style="white-space: nowrap;">Type</th>  
                        <th style="white-space: nowrap;">Manager</th>  
                        <th style="white-space: nowrap;">Lead</th>
                        <th style="white-space: nowrap;">Opened Date</th>      
                        <th style="white-space: nowrap;" class="no-sort notexport" data-priority="1"></th>                    
                    </tr>
                </thead>
                <tbody>                         
                </tbody>
            </table>   
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('css')
   <link rel="stylesheet" href="{{asset('vendor/datatables/css/dataTables.bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/datatables-plugins/responsive/css/responsive.bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/datatables-plugins/buttons/css/buttons.bootstrap4.min.css')}}">
@stop

@section('js')
   <script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
   <script src="{{asset('vendor/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js')}}"></script>

   <script src="{{asset('js/datatables-extentions.js')}}"></script>
   <script type="text/javascript">
      $(function () {
         $('#projects').DataTable({
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "fixedHeader": true,        
            'paging'      : true,
            'pageLength'  : 10,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'scrollX'     : false,
            'autoWidth'   : true, 
            ajax: {
               url: "{!! url('master-job/'.$masterJob->mjob_id.'/project_files/list-datatables') !!}",
            },
            "columns":[
               { "data": "proj_id" },
               { "data": "project_id" },
               { "data": "project_status" },  
               { "data": "project_type" },  
               { "data": "project_manager" },
               { "data": "project_lead" },
               { "data": "project_date_opened" },           
               { data: 'action', name: 'action', orderable: false, searchable: false,
                   render: function (data) { 
                     return '<form action="{!!url("contacts/'+data+'")!!}" method="POST" enctype="multipart/form-data" >'+
                           '{{csrf_field()}} @method("DELETE")'+
                           '<a href="{!!url("contacts/'+data+'/edit")!!}" class="btn btn-xs btn-custom"><i class="fas fa-fw fa-edit"></i></a> &nbsp;&nbsp;'+
                           '<a href="javascript:;" class="btn btn-danger btn-xs" data-click="swal-danger" ><i class="fa fa-trash-alt"></i></a>'+
                        '</form>';}
               }, 
            ],
            'order':[[0,'desc']],
            'columnDefs':[
               {"searchable": false, "targets": [0] },
               {orderable: false,targets: "no-sort"},
               {"targets": [0],visible: false},
             ],      
         })
      })
   </script>
   <script type="text/javascript">
      $("#contacts").on("click", "[data-click='swal-danger']", function (e) { 
         var form = $(this).parents('form');
         dangerNotification('Are You Sure?','You are about to delete this project file.','question','Delete',form)
      });
   </script>
@stop