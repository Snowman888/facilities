@extends('adminlte::page')

@section('title_prefix', 'Create Organization - ')

@section('content_header')
    <h1>Create Organization</h1>
@stop

@section('content')

   <div class="card card-navy">
      <div class="card-header">
         <h3 class="card-title">Create Organization</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
      </div>


      <div class="card-body">
         <div class="row justify-content-center">
            <div class="col-md-9 offset-md-3">
                <form class="form-horizontal" action="{{url('organizations')}}" method="POST" name="form" id="myForm" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    {{-- first name --}}
                    <div class="form-group row m-b-17 {{ $errors->has('org_name') ? 'has-error' : ''}}" >
                        <label for="org_name" class="col-md-1 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('org_name',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member', 
                                    'data-parsley-required'=>'true',
                                    'data-parsley-required-message'=>'Name is Required',
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('org_name'),
                                    'placeholder'=>'- -',
                                    'id'=>'org_name',
                                    'class'=>'form-control',
                                    'data-parsley-remote'=>url('validation/validateoptions'),
                                    'data-parsley-remote-message'=>"Organization Name Already Taken",
                                    'data-parsley-remote-options'=>'{ "type": "POST", "dataType": "json", "data": { "_token": "'.csrf_token().'","table" : "organizations" , "type":"uniqueness", "table_field":"org_name", "id":"new","filter_field":"org_id" } }'
                                    )
                            )!!}                        
                            @foreach ($errors->get('org_name') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Type --}}
                    <div class="form-group row m-b-17 {{ $errors->has('org_type') ? 'has-error' : ''}}" >
                        <label for="org_type" class="col-md-1 col-form-label">Type <span class="text-danger">*</span></label>
                        <div class="col-md-7 error-block">
                            {!!Form::select('org_type',
                                config('master.organizations.type'),
                                null,
                                array(
                                    'data-parsley-group'=>'step-member', 
                                    'data-parsley-required'=>'true',
                                    'data-parsley-required-message'=>'Please Select Type',
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('org_type'),
                                    'class'=>'form-control',
                                    'data-size'=>'10',
                                    'id'=>'client',
                                    'data-live-search'=>'false',
                                    'data-style'=>'btn-white',
                                    'tabindex'=>'-98',
                                    'placeholder'=>'--Select Type--'
                                    )
                            )!!}                                
                            @foreach ($errors->get('org_type') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- phone --}}
                    <div class="form-group row m-b-17 {{ $errors->has('org_phone') ? 'has-error' : ''}}" >
                        <label for="org_phone" class="col-md-1 col-form-label">Phone </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('org_phone',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                   
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('org_phone'),
                                    'placeholder'=>'- -',
                                    'id'=>'org_phone',
                                    'class'=>'form-control',
                                    'data-inputmask'=>"'mask':'(999) 999-9999'",
                                    'inputmode'=>"text"
                                    )
                            )!!}                        
                            @foreach ($errors->get('org_phone') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- fax --}}
                    <div class="form-group row m-b-17 {{ $errors->has('org_fax') ? 'has-error' : ''}}" >
                        <label for="org_fax" class="col-md-1 col-form-label">Fax </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('org_fax',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                   
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('org_fax'),
                                    'placeholder'=>'- -',
                                    'id'=>'org_fax',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('org_fax') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- email --}}
                    <div class="form-group row m-b-17 {{ $errors->has('org_email') ? 'has-error' : ''}}" >
                        <label for="org_email" class="col-md-1 col-form-label">Email </label>
                        <div class="col-md-7 error-block">
                            {!!Form::email('org_email',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                   
                                    'data-parsley-trigger'=> 'change focusout',
                                    'data-parsley-type-message'=>'Please Enter a Valid Email Address',
                                    'data-parsley-type'=>'email', 
                                    'value' => Input::old('org_email'),
                                    'placeholder'=>'- -',
                                    'id'=>'org_email',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('org_email') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- address --}}
                    <div class="form-group row m-b-17 {{ $errors->has('org_address') ? 'has-error' : ''}}" >
                        <label for="org_address" class="col-md-1 col-form-label">Address </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('org_address',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                   
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('org_address'),
                                    'placeholder'=>'- -',
                                    'id'=>'org_address',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('org_address') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-sm-2 col-md-7">
                            <button type="submit" class="btn btn-custom" id="submit_info">Create</button>
                        </div>
                    </div>
                </form>
            </div>

          </div>
      </div>
        <!-- /.card-body -->
   </div>
@stop

@section('css')
    
@stop

@section('js')
    <script src="{{asset('vendor/parsley/dist/parsley.js')}}"></script>  
    <script src="{{asset('vendor/inputmask/jquery.inputmask.min.js')}}"></script>  

    <script>
        // parsely

        $('#submit_info').on('click', function(e, anchorObject, stepNumber, stepDirection) {
            var res = $('form[name="form"]').parsley().validate('step-member');
            return res;
        });
        $("#org_phone").inputmask({"mask": "(999) 999-9999"});
    </script>

@stop
