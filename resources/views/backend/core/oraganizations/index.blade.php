@extends('adminlte::page')

@section('title_prefix', 'Organizations - ')

@section('content_header')
    <h1>Organizations</h1>
@stop

@section('content')
   <div class="card">
      <!-- /.card-header -->
      <div class="card-body">
         <div class="float-right">
            <a href="{{url('organizations/create')}}" class="btn btn-orange"><i class="fas fa-plus"></i> Add Organization</a>
         </div>
      </div>
     <!-- /.card-body -->
   </div>


<div class="card card-navy">
   <div class="card-header">
      <h3 class="card-title">Organizations</h3>

      <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
         </button>
      </div>
   </div>

   <div class="card-body">
      <table id="orgs" class="table table-bordered table-striped display" nowrap width="100%">
         <thead>
            <tr>         
               <th style="white-space: nowrap;">ID</th>
               <th style="white-space: nowrap;">Name</th>
               <th style="white-space: nowrap;">Type</th> 
               <th style="white-space: nowrap;">Phone</th>  
               <th style="white-space: nowrap;">Email</th>  
               <th style="white-space: nowrap;"># Master Jobs</th>
               <th style="white-space: nowrap;" class="no-sort">Address</th>          
               <th style="white-space: nowrap;" class="no-sort notexport" data-priority="1"></th>                    
            </tr>
         </thead>
         <tbody>                         
         </tbody>
         <tfoot>
            <tr>
               <th>ID</th> 
               <th>Name</th>             
               <th>Type</th>
               <th>Phone</th>
               <th>Email</th>
               <th># Master Jobs</th>
               <th>Address</th>
               <th data-priority="1"></th>    
            </tr>
         </tfoot>
      </table>   
   </div>
<!-- /.card-body -->

</div>
@stop

@section('css')
   <link rel="stylesheet" href="{{asset('vendor/datatables/css/dataTables.bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/datatables-plugins/responsive/css/responsive.bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/datatables-plugins/buttons/css/buttons.bootstrap4.min.css')}}">
@stop

@section('js')
   <script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
   <script src="{{asset('vendor/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js')}}"></script>

   <script src="{{asset('vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js')}}"></script>

   <script src="{{asset('js/datatables-extentions.js')}}"></script>
   <script type="text/javascript">
      $(function () {
         $('#orgs').DataTable({
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "fixedHeader": true,        
            'paging'      : true,
            'pageLength'  : 10,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'scrollX'     : false,
            'autoWidth'   : true, 
            ajax: {
               url: "{!! url('organizations/list-datatables') !!}",
            },
            "columns":[
               { "data": "org_id" },
               { "data": "org_name" },
               { "data": "org_type" },  
               { "data": "org_phone" },  
               { "data": "org_email" },
               { "data": "master_jobs_count" },
               { "data": "org_address" },            
               { data: 'action', name: 'action', orderable: false, searchable: false,
                   render: function (data) { 
                     return '<form action="{!!url("organizations/'+data+'")!!}" method="POST" enctype="multipart/form-data" >'+
                           '{{csrf_field()}} @method("DELETE")'+
                           '<a href="{!!url("organizations/'+data+'/edit")!!}" class="btn btn-xs btn-custom"><i class="fas fa-fw fa-edit"></i></a> &nbsp;&nbsp;'+
                           '<a href="javascript:;" class="btn btn-danger btn-xs" data-click="swal-danger" ><i class="fa fa-trash-alt"></i></a>'+
                        '</form>';}
               }, 
            ],
            'order':[[0,'desc']],
            'columnDefs':[
               {"searchable": false, "targets": [7,6,5,0] },
               {orderable: false,targets: "no-sort"},
               {"targets": [0],visible: false},
            ],
            // dom: 'lBfrtip',
            // buttons: 
            // [
              //  {
              //     text: '<i class="fas fa-plus-circle"></i> Add User',
              //     className: 'btn-sm btn btn-green addservicessBtn',
              //     action: function ( e, dt, node, config ) {
              //        onclick (window.location.href="{{url('agency/add-user')}}")
              //     }
              //  }
            // ],
      
         })
      })
   </script>
   <script type="text/javascript">
      $("#orgs").on("click", "[data-click='swal-danger']", function (e) { 
         var form = $(this).parents('form');
         dangerNotification('Are You Sure?','You are about to delete this organization.','question','Delete',form)
      });
   </script>
@stop
