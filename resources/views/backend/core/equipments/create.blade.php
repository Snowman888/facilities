@extends('adminlte::page')

@section('title_prefix', 'Add Equipment - ')

@section('content_header')
    <h1>Add Equipment</h1>
@stop

@section('content')

   <div class="card card-navy">
        <div class="card-header">
            <h3 class="card-title">Add Equipment</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="row justify-content-center">
            <div class="col-md-9 offset-md-3">
                <form class="form-horizontal" action="{{url('equipments')}}" method="POST" name="form" id="myForm" enctype="multipart/form-data">
                    @csrf
                    @method('POST')

                    {{-- ID --}}
                    <div class="form-group row m-b-17 {{ $errors->has('equip_e_id') ? 'has-error' : ''}}" >
                        <label for="equip_e_id" class="col-md-2 col-form-label">ID <span class="text-danger">*</span></label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('equip_e_id',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',  
                                    'data-parsley-required'=>'true',                                 
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('equip_e_id'),
                                    'placeholder'=>'- -',
                                    'id'=>'equip_e_id',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('equip_e_id') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Status --}}
                    <div class="form-group row m-b-17 {{ $errors->has('equip_status') ? 'has-error' : ''}}" >
                        <label for="equip_status" class="col-md-2 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-md-7 error-block">
                            {!!Form::select('equip_status',
                                config('master.equipments.status'),
                                null,
                                array(
                                    'data-parsley-group'=>'step-member', 
                                    'data-parsley-required'=>'true',
                                    'data-parsley-required-message'=>'Please Select Status',
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('equip_status'),
                                    'class'=>'form-control',
                                    'data-size'=>'10',
                                    'id'=>'client',
                                    'data-live-search'=>'false',
                                    'data-style'=>'btn-white',
                                    'tabindex'=>'-98',
                                    'placeholder'=>'--Select Status--'
                                    )
                            )!!}                                
                            @foreach ($errors->get('equip_status') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Type --}}
                    <div class="form-group row m-b-17 {{ $errors->has('equip_type') ? 'has-error' : ''}}" >
                        <label for="equip_type" class="col-md-2 col-form-label">Type </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('equip_type',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                  
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('equip_type'),
                                    'placeholder'=>'- -',
                                    'id'=>'equip_type',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('equip_type') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Mfg --}}
                    <div class="form-group row m-b-17 {{ $errors->has('equip_mfg') ? 'has-error' : ''}}" >
                        <label for="equip_mfg" class="col-md-2 col-form-label">Mfg </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('equip_mfg',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('equip_mfg'),
                                    'placeholder'=>'- -',
                                    'id'=>'equip_mfg',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('equip_mfg') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Model --}}
                    <div class="form-group row m-b-17 {{ $errors->has('equip_model') ? 'has-error' : ''}}" >
                        <label for="equip_model" class="col-md-2 col-form-label">Model </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('equip_model',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                  
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('equip_model'),
                                    'placeholder'=>'- -',
                                    'id'=>'equip_model',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('equip_model') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Serial --}}
                    <div class="form-group row m-b-17 {{ $errors->has('equip_serial') ? 'has-error' : ''}}" >
                        <label for="equip_serial" class="col-md-2 col-form-label">Serial </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('equip_serial',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                  
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('equip_serial'),
                                    'placeholder'=>'- -',
                                    'id'=>'equip_serial',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('equip_serial') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Purchase Date --}}
                    <div class="form-group row m-b-17 {{ $errors->has('equip_p_date') ? 'has-error' : ''}}" >
                        <label for="equip_p_date" class="col-md-2 col-form-label">Purchase Date </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('equip_p_date',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                  
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('equip_p_date'),
                                    'placeholder'=>'- -',
                                    'id'=>'equip_p_date',
                                    'class'=>'form-control',
                                    'data-inputmask-alias'=>"datetime",
                                    'data-inputmask-inputformat'=>"mm/dd/yyyy",
                                    'inputmode'=>"numeric",
                                    'data-date-format'=>'mm/dd/yyyy'

                                    )
                            )!!}                        
                            @foreach ($errors->get('equip_p_date') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Purchase Cost --}}
                    <div class="form-group row m-b-17 {{ $errors->has('equip_p_cost') ? 'has-error' : ''}}" >
                        <label for="equip_p_cost" class="col-md-2 col-form-label">Purchase Cost </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('equip_p_cost',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                  
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('equip_p_cost'),
                                    'placeholder'=>'- -',
                                    'id'=>'equip_p_cost',
                                    'class'=>'form-control',
                                    'data-parsley-pattern'=>'/^[0-9]*(\.[0-9]{0,2})?$/',
                                    'data-parsley-pattern-message'=>'Invalid value. Excepted format eg: -55.22 or 55',
                                    'onkeypress'=>'return validateFloatKeyPress(this,event);'
                                    )
                            )!!}                        
                            @foreach ($errors->get('equip_p_cost') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Vendor --}}
                    <div class="form-group row m-b-17 {{ $errors->has('equip_vendor') ? 'has-error' : ''}}" >
                        <label for="equip_vendor" class="col-md-2 col-form-label">Vendor </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('equip_vendor',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                  
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('equip_vendor'),
                                    'placeholder'=>'- -',
                                    'id'=>'equip_vendor',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('equip_vendor') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-sm-2 col-md-7">
                            <button type="submit" class="btn btn-custom" id="submit_info">Create</button>
                        </div>
                    </div>
                </form>
            </div>

          </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('css')
    
@stop

@section('js')
    <script src="{{asset('vendor/parsley/dist/parsley.js')}}"></script>  
    <script src="{{asset('vendor/inputmask/jquery.inputmask.min.js')}}"></script>
    <script src="{{asset('js/restrict-decimalplaces.js')}}"></script>

    <script type="text/javascript">
        $('#submit_info').on('click', function(e, anchorObject, stepNumber, stepDirection) {
            var res = $('form[name="form"]').parsley().validate('step-member');
            return res;
        });
    </script>

    <script type="text/javascript">
        $("#equip_p_date").inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
    </script>
@stop
