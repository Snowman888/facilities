@extends('adminlte::page')

@section('title_prefix', 'Equipments - ')

@section('content_header')
    <h1>Equipments</h1>
@stop

@section('content')
    
    <div class="card">
      <!-- /.card-header -->
        <div class="card-body">
            <div class="float-right">
                <a href="{{url('equipments/create')}}" class="btn btn-orange"><i class="fas fa-plus"></i> Add Equipment</a>
            </div>
        </div>
     <!-- /.card-body -->
    </div>

    <div class="card card-navy">
        <div class="card-header">
            <h3 class="card-title">Equipments</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <table id="equips" class="table table-bordered table-striped display" width="100%">
                <thead>
                    <tr>         
                        <th style="white-space: nowrap;">Main_ID</th>
                        <th style="white-space: nowrap;">ID</th>
                        <th style="white-space: nowrap;">Status</th>
                        <th style="white-space: nowrap;">Type</th>
                        <th style="white-space: nowrap;">Mfg</th> 
                        <th style="white-space: nowrap;">Model</th>  
                        <th style="white-space: nowrap;">Serial</th>  
                        <th style="white-space: nowrap;">Purchase Date</th>
                        <th style="white-space: nowrap;">Cost</th>
                        <th style="white-space: nowrap;">Vendor</th>        
                        <th style="white-space: nowrap;" class="no-sort notexport" data-priority="1"></th>                    
                    </tr>
                </thead>
                <tbody>                         
                </tbody>             
            </table>   
        </div>
        <!-- /.card-body -->
    </div>


@stop


@section('css')
   <link rel="stylesheet" href="{{asset('vendor/datatables/css/dataTables.bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/datatables-plugins/responsive/css/responsive.bootstrap4.min.css')}}">
   <style type="text/css">
   
   </style>
@stop

@section('js')
   <script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
   <script src="{{asset('vendor/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js')}}"></script>

   <script src="{{asset('js/datatables-extentions.js')}}"></script>

   <script type="text/javascript">
      $(function () {
         $('#equips').DataTable({
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "fixedHeader": true,        
            'paging'      : true,
            'pageLength'  : 10,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'scrollX'     : false,
            'autoWidth'   : true, 
            ajax: {
               url: "{!! url('equipments/list-datatables') !!}",
            },
            "columns":[
               { "data": "equip_id" },
               { "data": "equip_e_id" },
               { "data": "equip_status" },
               { "data": "equip_type" },  
               { "data": "equip_mfg" },  
               { "data": "equip_model" },
               { "data": "equip_serial" },
               { "data": "equip_p_date" },
               { "data": "equip_p_cost" },   
               { "data": "equip_vendor" },               
               { data: 'action', name: 'action', orderable: false, searchable: false,
                   render: function (data) { 
                     return '<form action="{!!url("equipments/'+data+'")!!}" method="POST" enctype="multipart/form-data" >'+
                           '{{csrf_field()}} @method("DELETE")'+
                           '<a href="{!!url("equipments/'+data+'/edit")!!}" class="btn btn-xs btn-custom"><i class="fas fa-fw fa-edit"></i></a> &nbsp;&nbsp;'+
                           '<a href="javascript:;" class="btn btn-danger btn-xs" data-click="swal-danger" ><i class="fa fa-trash-alt"></i></a>'+
                        '</form>';}
               }, 
            ],
            'order':[[0,'desc']],
            'columnDefs':[  
               {orderable: false,targets: "no-sort"},
               {"targets": [0],visible: false},
               { width: "10%", targets: 10 },
               {"targets": [10],"className": "text-center nowrap"},
            ],
         })
      })
   </script>
   <script type="text/javascript">
      $("#equips").on("click", "[data-click='swal-danger']", function (e) { 
         var form = $(this).parents('form');
         dangerNotification('Are You Sure?','You are about to delete this equipment. Some modules might be using this.','question','Delete',form)
      });
   </script>
@stop
