@extends('adminlte::page')

@section('title_prefix', 'Contacts - ')

@section('content_header')
    <h1>Contacts</h1>
@stop

@section('content')
    
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <div class="float-right">
                <a href="{{url('contacts/create')}}" class="btn btn-orange"><i class="fas fa-plus"></i> Add Contact</a>
            </div>
        </div>
    </div>


   <div class="card card-navy">
   <div class="card-header">
      <h3 class="card-title">Contacts</h3>

      <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
         </button>
      </div>
   </div>

   <div class="card-body">
      <table id="contacts" class="table table-bordered table-striped display" nowrap width="100%">
         <thead>
            <tr>         
               <th style="white-space: nowrap;">ID</th>
               <th style="white-space: nowrap;">First Name</th>
               <th style="white-space: nowrap;">Last Name</th> 
               <th style="white-space: nowrap;">Type</th>  
               <th style="white-space: nowrap;">Organization</th>  
               <th style="white-space: nowrap;">Email</th>
               <th style="white-space: nowrap;">Home</th>
               <th style="white-space: nowrap;">Cell</th>
               <th style="white-space: nowrap;">Work</th>         
               <th style="white-space: nowrap;" class="no-sort notexport" data-priority="1"></th>                    
            </tr>
         </thead>
         <tbody>                         
         </tbody>
      </table>   
   </div>
<!-- /.card-body -->

</div>
@stop

@section('css')
   <link rel="stylesheet" href="{{asset('vendor/datatables/css/dataTables.bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/datatables-plugins/responsive/css/responsive.bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/datatables-plugins/buttons/css/buttons.bootstrap4.min.css')}}">
@stop

@section('js')
   <script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
   <script src="{{asset('vendor/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js')}}"></script>

   <script src="{{asset('vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js')}}"></script>
   <script src="{{asset('vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js')}}"></script>

   <script src="{{asset('js/datatables-extentions.js')}}"></script>
   <script type="text/javascript">
      $(function () {
         $('#contacts').DataTable({
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "fixedHeader": true,        
            'paging'      : true,
            'pageLength'  : 10,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'scrollX'     : false,
            'autoWidth'   : true, 
            ajax: {
               url: "{!! url('contacts/list-datatables') !!}",
            },
            "columns":[
               { "data": "c_id" },
               { "data": "c_first_name" },
               { "data": "c_last_name" },  
               { "data": "c_type" },  
               { "data": "org" },
               { "data": "c_email" },
               { "data": "c_home" },    
               { "data": "c_cell" }, 
               { "data": "c_work" },         
               { data: 'action', name: 'action', orderable: false, searchable: false,
                   render: function (data) { 
                     return '<form action="{!!url("contacts/'+data+'")!!}" method="POST" enctype="multipart/form-data" >'+
                           '{{csrf_field()}} @method("DELETE")'+
                           '<a href="{!!url("contacts/'+data+'/edit")!!}" class="btn btn-xs btn-custom"><i class="fas fa-fw fa-edit"></i></a> &nbsp;&nbsp;'+
                           '<a href="javascript:;" class="btn btn-danger btn-xs" data-click="swal-danger" ><i class="fa fa-trash-alt"></i></a>'+
                        '</form>';}
               }, 
            ],
            'order':[[0,'desc']],
            'columnDefs':[
               {"searchable": false, "targets": [0] },
               {orderable: false,targets: "no-sort"},
               {"targets": [0],visible: false},
            ],      
         })
      })
   </script>
   <script type="text/javascript">
      $("#contacts").on("click", "[data-click='swal-danger']", function (e) { 
         var form = $(this).parents('form');
         dangerNotification('Are You Sure?','You are about to delete this organization.','question','Delete',form)
      });
   </script>
@stop