@extends('adminlte::page')

@section('title_prefix', 'Create Contact - ')

@section('content_header')
    <h1>Create Contact</h1>
@stop

@section('content')

   <div class="card card-navy">
      <div class="card-header">
         <h3 class="card-title">Create Contact</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
      </div>


      <div class="card-body">
         <div class="row justify-content-center">
            <div class="col-md-9 offset-md-2">
                <form class="form-horizontal" action="{{route('contacts.store')}}" method="POST" name="form" id="myForm" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    {{-- first name --}}
                    <div class="form-group row m-b-17 {{ $errors->has('c_first_name') ? 'has-error' : ''}}" >
                        <label for="c_first_name" class="col-md-2 col-form-label">First Name <span class="text-danger">*</span></label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('c_first_name',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member', 
                                    'data-parsley-required'=>'true',
                                    'data-parsley-required-message'=>'First Name is Required',
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('c_first_name'),
                                    'placeholder'=>'- -',
                                    'id'=>'c_first_name',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('c_first_name') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Last name --}}
                    <div class="form-group row m-b-17 {{ $errors->has('c_last_name') ? 'has-error' : ''}}" >
                        <label for="c_last_name" class="col-md-2 col-form-label">Last Name <span class="text-danger">*</span></label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('c_last_name',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member', 
                                    'data-parsley-required'=>'true',
                                    'data-parsley-required-message'=>'Last Name is Required',
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('c_last_name'),
                                    'placeholder'=>'- -',
                                    'id'=>'c_last_name',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('c_last_name') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Type --}}
                    <div class="form-group row m-b-17 {{ $errors->has('c_type') ? 'has-error' : ''}}" >
                        <label for="c_type" class="col-md-2 col-form-label">Type </label>
                        <div class="col-md-7 error-block">
                            {!!Form::select('c_type',
                                config('master.contacts.type'),
                                null,
                                array(                                    
                                    'value' => Input::old('c_type'),
                                    'class'=>'form-control',
                                    'data-size'=>'10',
                                    'id'=>'client',
                                    'data-live-search'=>'false',
                                    'data-style'=>'btn-white',
                                    'tabindex'=>'-98',
                                    'placeholder'=>'--Select Type--'
                                    )
                            )!!}                                
                            @foreach ($errors->get('c_type') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Organization --}}
                    <div class="form-group row m-b-17 {{ $errors->has('organization_id') ? 'has-error' : ''}}" >
                        <label for="organization_id" class="col-md-2 col-form-label">Organization </label>
                        <div class="col-md-7 error-block">
                            {!!Form::select('organization_id',
                                $orgs,
                                null,
                                array(                                    
                                    'value' => Input::old('organization_id'),
                                    'class'=>'form-control organization_id',
                                    'data-size'=>'10',
                                    'id'=>'client',
                                    'data-live-search'=>'false',
                                    'data-style'=>'btn-white',
                                    'tabindex'=>'-98',
                                    'placeholder'=>'--Select Type--'
                                    )
                            )!!}                                
                            @foreach ($errors->get('organization_id') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>                  

                    {{-- email --}}
                    <div class="form-group row m-b-17 {{ $errors->has('c_email') ? 'has-error' : ''}}" >
                        <label for="c_email" class="col-md-2 col-form-label">Email </label>
                        <div class="col-md-7 error-block">
                            {!!Form::email('c_email',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                   
                                    'data-parsley-trigger'=> 'change focusout',
                                    'data-parsley-type-message'=>'Please Enter a Valid Email Address',
                                    'data-parsley-type'=>'email', 
                                    'value' => Input::old('c_email'),
                                    'placeholder'=>'- -',
                                    'id'=>'c_email',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('c_email') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Home --}}
                    <div class="form-group row m-b-17 {{ $errors->has('c_home') ? 'has-error' : ''}}" >
                        <label for="c_home" class="col-md-2 col-form-label">Home </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('c_home',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                   
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('c_home'),
                                    'placeholder'=>'- -',
                                    'id'=>'c_home',
                                    'class'=>'form-control phone',
                                    'data-inputmask'=>"'mask':'(999) 999-9999'",
                                    'inputmode'=>"text"
                                    )
                            )!!}                        
                            @foreach ($errors->get('c_home') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Cell --}}
                    <div class="form-group row m-b-17 {{ $errors->has('c_cell') ? 'has-error' : ''}}" >
                        <label for="c_cell" class="col-md-2 col-form-label">Cell </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('c_cell',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                   
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('c_cell'),
                                    'placeholder'=>'- -',
                                    'id'=>'c_cell',
                                    'class'=>'form-control phone',
                                    'data-inputmask'=>"'mask':'(999) 999-9999'",
                                    'inputmode'=>"text"
                                    )
                            )!!}                        
                            @foreach ($errors->get('c_cell') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Work --}}
                    <div class="form-group row m-b-17 {{ $errors->has('c_work') ? 'has-error' : ''}}" >
                        <label for="c_work" class="col-md-2 col-form-label">Work </label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('c_work',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',                                   
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('c_work'),
                                    'placeholder'=>'- -',
                                    'id'=>'c_work',
                                    'class'=>'form-control phone',
                                    'data-inputmask'=>"'mask':'(999) 999-9999'",
                                    'inputmode'=>"text"
                                    )
                            )!!}                        
                            @foreach ($errors->get('c_work') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>
                    

                    <div class="form-group row">
                        <div class="offset-sm-2 col-md-7">
                            <button type="submit" class="btn btn-custom" id="submit_info">Create</button>
                        </div>
                    </div>
                </form>
            </div>

          </div>
      </div>
        <!-- /.card-body -->
   </div>
@stop

@section('css')
    <link href="{{asset('vendor/select2/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}" rel="stylesheet">
@stop

@section('js')
    <script src="{{asset('vendor/parsley/dist/parsley.js')}}"></script>  
    <script src="{{asset('vendor/inputmask/jquery.inputmask.min.js')}}"></script> 
    <script src="{{asset('vendor/select2/js/select2.min.js')}}"></script> 

    <script>
        // parsely

        $('#submit_info').on('click', function(e, anchorObject, stepNumber, stepDirection) {
            var res = $('form[name="form"]').parsley().validate('step-member');
            return res;
        });
        $(".phone").inputmask({"mask": "(999) 999-9999"});
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.organization_id').select2({
                placeholder: "- -",
                allowClear: true
            });
        });
    </script>

@stop
