{{-- Info Page --}}

<script type="text/javascript">

	//putting image name on file form
	$("input[type=file]").change(function () {
		var fieldVal = $(this).val();

		   // Change the node's value by removing the fake path (Chrome)
		   fieldVal = fieldVal.replace("C:\\fakepath\\", "");

		   if (fieldVal != undefined || fieldVal != "") {
		     	$(this).next(".custom-file-label").attr('data-content', fieldVal);
		     	$(this).next(".custom-file-label").text(fieldVal);
		   }
	});


	// parsely for info
	$('#submit_info').on('click', function(e, anchorObject, stepNumber, stepDirection) {
    	var res = $('form[name="form-assign-member"]').parsley().validate('step-member');
    	return res;
  	});

</script>



{{-- Profile Page --}}

<script type="text/javascript">
$('[data-click="swal-danger"]').click(function(e) {
	var form = $(this).parents('form');
	dangerNotification('Are You Sure?','Once DeActivated You will Not Be Able To Login Again.','question','Yes, DeActivate!',form)
});

</script>