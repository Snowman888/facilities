<form action="{{url('settings/change-password')}}"  method="POST" name="form-change-password" enctype="multipart/form-data">
	{{csrf_field()}}
	<div class="form-group row m-b-15 {{ $errors->has('current_password') ? 'has-error' : ''}}">
		<label class="col-md-3 col-form-label">Current Password <span class="text-danger">*</span> 					
		</label>
		<div class="col-md-7">
			<input 
				data-toggle="password" 
				data-placement="after" 
				type="password" 
				name="current_password"  
				id="current_password"
				class="form-control"  
				placeholder="Current Password *" 
				value="{{ old('current_password') }}"
				data-parsley-group='step-password' 
            data-parsley-required='true'
            data-parsley-required-message='Current password is required'
            data-parsley-errors-container="#current_password-error"
			>
			<span id="current_password-error"></span>
			   @foreach ($errors->get('current_password') as $message) 
					<p class="help-block">{{$message}}</p>
				@endforeach					   
		</div>				  	
	</div>
	<div class="form-group row m-b-15 {{ $errors->has('password') ? 'has-error' : ''}}">
		<label class="col-md-3 col-form-label">New Password <span class="text-danger">*</span>
			<div class="popup" onclick="myFunction()"><i class="fas fa-info-circle"></i>
				<span class="popuptext" id="myPopup">Password must have:<br/>
				  	1 Capital Letter<br/>1 Lower Case<br/>1 Number<br/>1 Unique Character<br/>8 Minimum words
				</span>		
			</div>			
		</label>
		<div class="col-md-7">
			<input 
				data-toggle="password" 
				data-placement="after" 
				type="password" 
				name="password" 
				id="password"
				class="form-control"  
				placeholder="New Password *" 
				value="{{ old('password') }}"
				data-parsley-group='step-password' 
            data-parsley-required='true'
            data-parsley-required-message='New password is required'
            data-parsley-errors-container="#new_password-error"
            data-parsley-pattern="/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/"
            data-parsley-pattern-message='New Password must contain at least 1 uppercase, 1 lowercase, 1 number and 1 special character'
           
			>
				<span id="new_password-error"></span>
			   @foreach ($errors->get('password') as $message) 
					<p class="help-block">{{$message}}</p>
				@endforeach					   
		</div>				  	
	</div>					
	<div class="form-group row m-b-15 {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
		<label class="col-md-3 col-form-label">Confirm New Password <span class="text-danger">*</span>					
		</label>
		<div class="col-md-7">
			<input 
				data-toggle="password" 
				data-placement="after" 
				type="password" 
				name="password_confirmation" 
			 	id="password_confirmation"
				class="form-control"  
				placeholder="Confirm Password *" 
				value="{{ old('password_confirmation') }}"
				data-parsley-group='step-password' 
            data-parsley-required='true'
            data-parsley-required-message='Password Confirmation is required'
            data-parsley-errors-container="#password_confirmation-error"
            data-parsley-equalTo="#password"
            data-parsley-equalTo-message="Confirmation Password Did Not Match"
			>
				<span id="password_confirmation-error"></span>
			   @foreach ($errors->get('password_confirmation') as $message) 
					<p class="help-block">{{$message}}</p>
				@endforeach					   
		</div>				  	
	</div>	
	<center><button type="submit" id="submit_password" class="btn btn-custom">Update Password</button><center>				
	
</form>

@push('js')
	<script>
		// When the user clicks on div, open the popup
		function myFunction() {
		  var popup = document.getElementById("myPopup");
		  popup.classList.toggle("show");
		}

		$('#submit_password').on('click', function(e, anchorObject, stepNumber, stepDirection) {
	    	var res = $('form[name="form-change-password"]').parsley().validate('step-password');
	    	return res;
	  	});

	</script>
@endpush