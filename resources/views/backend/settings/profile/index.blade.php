@extends('adminlte::page')

@section('title_prefix', 'Profile - ')

@section('content_header')
<h1>Profile</h1>
@stop

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			@include('backend.settings.profile.profile')			
		</div>
		<!-- /.col -->
		<div class="col-md-9">
			<div class="card card-navy card-outline card-outline-tabs">
				<div class="card-header p-0 border-bottom-0">
					<ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link {{!$errors->get('current_password') && !$errors->get('password') && !$errors->get('password_confirmation') ?'active':''}}"  data-toggle="pill" href="#info" role="tab" aria-controls="info" aria-selected="true">Info</a>
						</li>
						<li class="nav-item">
							<a class="nav-link {{$errors->get('current_password') || $errors->get('password') || $errors->get('password_confirmation') ?'active':''}}"  data-toggle="pill" href="#change_password" role="tab" aria-controls="change_password" aria-selected="true">Change Password</a>
						</li>					
					</ul>
				</div>
				<div class="card-body">
					<div class="tab-content">
						<div class="tab-pane fade {{!$errors->get('current_password') && !$errors->get('password') && !$errors->get('password_confirmation') ?'active show':''}}" id="info" role="tabpanel" aria-labelledby="info">
							@include('backend.settings.profile.info')
						</div>
						<div class="tab-pane fade {{$errors->get('current_password') || $errors->get('password') || $errors->get('password_confirmation') ?'active show':''}}" id="change_password" role="tabpanel" aria-labelledby="change_password">
							@include('backend.settings.profile.change-password')
						</div>
					</div>
				</div>
				<!-- /.card -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</div>

@stop

@section('css')
	@include('backend.settings.profile.assets.css')
@stop

@section('js')
	<script src="{{asset('vendor/parsley/dist/parsley.js')}}"></script>
	<script src="{{asset('js/parsely-image-validation.js')}}"></script>
	
	<script src="{{asset('vendor/bootstrap-show-password/bootstrap-show-password.min.js')}}"></script>
	@include('backend.settings.profile.assets.js')
	@include('backend.validation.parsely-unique-validation')
@stop
