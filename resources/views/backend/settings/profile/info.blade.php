<form class="form-horizontal" method="POST" name="form-assign-member" id="myForm" enctype="multipart/form-data">
@csrf

    {{-- Logo --}}
    <div class="form-group row {{ $errors->has('logo') ? 'has-error' : ''}}" >
        <label for="logo" class="col-sm-2 col-form-label">Change Logo </label>
        <div class="col-sm-10 error-block">
            <input type="file" data-parsley-filemaxmegabytes="2" data-parsley-errors-container="#profilelogolable" data-parsley-filemimetypes="image/jpeg, image/jpg,image/bmp, image/png" data-parsley-trigger="change" data-parsley-group="step-member"  name="logo" class="text-center center-block file-upload custom-file-input " id="profilelogo" >
            <label class="custom-file-label"  for="profilelogo">Choose image</label>
            <div id="profilelogolable"></div>
            @foreach ($errors->get('logo') as $message) 
              <p class="help-block">{{$message}}</p>
            @endforeach
        </div>
    </div>

    {{-- first name --}}
    <div class="form-group row is-invalid {{ $errors->has('first_name') ? 'has-error' : ''}}" >
        <label for="first_name" class="col-sm-2 col-form-label">First Name <span class="text-danger">*</span></label>
        <div class="col-sm-10 error-block">
            {!!Form::text('first_name',                      
                auth()->user()->first_name,
                array(
                    'data-parsley-group'=>'step-member', 
                    'data-parsley-required'=>'true',
                    'data-parsley-required-message'=>'First Name is Required',
                    'data-parsley-trigger'=> 'change focusout',
                    'value' => Input::old('first_name'),
                    'placeholder'=>'First Name',
                    'id'=>'first_name',
                    'class'=>'form-control',
                    )
            )!!}                        
            @foreach ($errors->get('first_name') as $message) 
              <p class="help-block">{{$message}}</p>
            @endforeach
        </div>
    </div>

    {{-- Last Name --}}
    <div class="form-group row {{ $errors->has('last_name') ? 'has-error' : ''}}" >
        <label for="last_name" class="col-sm-2 col-form-label">Last Name <span class="text-danger">*</span></label>
        <div class="col-sm-10 error-block">
            {!!Form::text('last_name',                      
                auth()->user()->last_name,
                array(
                    'data-parsley-group'=>'step-member', 
                    'data-parsley-required'=>'true',
                    'data-parsley-required-message'=>'Last Name is Required',
                    'data-parsley-trigger'=> 'change focusout',
                    'value' => Input::old('last_name'),
                    'placeholder'=>'Last Name',
                    'id'=>'last_name',
                    'class'=>'form-control',
                    )
            )!!}                        
            @foreach ($errors->get('last_name') as $message) 
              <p class="help-block">{{$message}}</p>
            @endforeach
        </div>
    </div>

    {{-- Title --}}
    <div class="form-group row {{ $errors->has('title') ? 'has-error' : ''}}" >
        <label for="title" class="col-sm-2 col-form-label">Title</label>
        <div class="col-sm-10 error-block">
            {!!Form::text('title',                      
                auth()->user()->title,
                array(
                    'data-parsley-group'=>'step-member', 
                    'data-parsley-required-message'=>'Title is Required',
                    'data-parsley-trigger'=> 'change focusout',
                    'value' => Input::old('title'),
                    'placeholder'=>'Title',
                    'id'=>'title',
                    'class'=>'form-control',
                    )
            )!!}                        
            @foreach ($errors->get('title') as $message) 
              <p class="help-block">{{$message}}</p>
            @endforeach
        </div>
    </div>

    {{-- Email --}}
    <div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}" >
        <label for="email" class="col-sm-2 col-form-label">Email <span class="text-danger">*</span></label>
        <div class="col-sm-10 error-block">
            {!! Form::email('email', 
                auth()->user()->email,
                array(
                    'data-parsley-group'=>'step-member',
                    'data-parsley-required'=>'true',
                    'data-parsley-required-message'=>'Email is required',
                    'data-parsley-type-message'=>'Please Enter a Valid Email Address',
                    'data-parsley-type'=>'email',                               
                    'placeholder'=>'Email Address', 
                    'id'=>'email',
                    'data-parsley-unique_email'=>auth()->user()->id,
                    'data-parsley-unique_email-message'=>'Sorry, this email is already being used. Please enter a new email',
                    'class' => 'form-control', 
                    'value' => Input::old('email')
            )) !!}                    
            @foreach ($errors->get('email') as $message) 
              <p class="help-block">{{$message}}</p>
            @endforeach
        </div>
    </div>

    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
            <button type="submit" class="btn btn-custom" id="submit_info">Update</button>
        </div>
    </div>
</form>