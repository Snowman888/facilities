<!-- Profile Image -->
<div class="card card-navy card-outline">
	<div class="card-body box-profile">
		<div class="text-center">
			<img class="profile-user-img img-fluid img-circle swalDefaultSuccess" src="{{ Auth::user()->adminlte_image() }}" alt="Profile Pic">
		</div>

		<h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

		<p class="text-muted text-center">{{Auth::user()->getRoleNames()->first()}}</p>

		<ul class="list-group list-group-unbordered mb-3">
			<li class="list-group-item">
				<b>Followers</b> <a class="float-right">1,322</a>
			</li>
			<li class="list-group-item">
				<b>Following</b> <a class="float-right">543</a>
			</li>
			<li class="list-group-item">
				<b>Friends</b> <a class="float-right">13,287</a>
			</li>
		</ul>
		<form action="{{url('settings/profile/delete')}}" method="POST">
			@method('DELETE')
			@csrf
			<button type="button" data-click="swal-danger" class="btn btn-danger btn-block" >DeActivate Account</button>
		</form>
		
	</div>
	<!-- /.card-body -->
</div>
<!-- /.card -->
<!-- /.card -->