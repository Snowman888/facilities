@extends('adminlte::page')

@section('title_prefix', 'Create User - ')

@section('content_header')
    <h1>Create User</h1>
@stop

@section('content')

   <div class="card card-navy">
        <div class="card-header">
          <h3 class="card-title">Create user</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="row justify-content-center">
            <div class="col-md-9 offset-md-3">
                <form class="form-horizontal" method="POST" name="form" id="myForm" enctype="multipart/form-data">
                    @csrf

                    {{-- Logo --}}
                    <div class="form-group row m-b-17 {{ $errors->has('logo') ? 'has-error' : ''}}" >
                        <label for="logo" class="col-md-2 col-form-label">Logo </label>
                        <div class="col-md-7 error-block">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="exampleInputFile" data-parsley-filemaxmegabytes="2" data-parsley-errors-container="#profilelogolable" data-parsley-filemimetypes="image/jpeg, image/jpg,image/bmp, image/png" data-parsley-trigger="change" data-parsley-group="step-member"  name="logo">
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                            <div id="profilelogolable"></div>
                            @foreach ($errors->get('logo') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- first name --}}
                    <div class="form-group row m-b-17 {{ $errors->has('first_name') ? 'has-error' : ''}}" >
                        <label for="first_name" class="col-md-2 col-form-label">First Name <span class="text-danger">*</span></label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('first_name',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member', 
                                    'data-parsley-required'=>'true',
                                    'data-parsley-required-message'=>'First Name is Required',
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('first_name'),
                                    'placeholder'=>'First Name',
                                    'id'=>'first_name',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('first_name') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Last Name --}}
                    <div class="form-group row m-b-17 {{ $errors->has('last_name') ? 'has-error' : ''}}" >
                        <label for="last_name" class="col-md-2 col-form-label">Last Name <span class="text-danger">*</span></label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('last_name',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member', 
                                    'data-parsley-required'=>'true',
                                    'data-parsley-required-message'=>'Last Name is Required',
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('last_name'),
                                    'placeholder'=>'Last Name',
                                    'id'=>'last_name',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('last_name') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Title --}}
                    <div class="form-group row m-b-17 {{ $errors->has('title') ? 'has-error' : ''}}" >
                        <label for="title" class="col-md-2 col-form-label">Title</label>
                        <div class="col-md-7 error-block">
                            {!!Form::text('title',                      
                                null,
                                array(
                                    'data-parsley-group'=>'step-member', 
                                    'data-parsley-required-message'=>'Title is Required',
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('title'),
                                    'placeholder'=>'Title',
                                    'id'=>'title',
                                    'class'=>'form-control',
                                    )
                            )!!}                        
                            @foreach ($errors->get('title') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Email --}}
                    <div class="form-group row m-b-17 {{ $errors->has('email') ? 'has-error' : ''}}" >
                        <label for="email" class="col-md-2 col-form-label">Email <span class="text-danger">*</span></label>
                        <div class="col-md-7 error-block">
                            {!! Form::email('email', 
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',
                                    'data-parsley-required'=>'true',
                                    'data-parsley-required-message'=>'Email is required',
                                    'data-parsley-type-message'=>'Please Enter a Valid Email Address',
                                    'data-parsley-type'=>'email',                               
                                    'placeholder'=>'Email Address', 
                                    'id'=>'email',
                                    'data-parsley-unique_email'=>'new',
                                    'data-parsley-unique_email-message'=>'Sorry, this email is already being used. Please enter a new email',
                                    'class' => 'form-control', 
                                    'value' => Input::old('email')
                            )) !!}                    
                            @foreach ($errors->get('email') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Email --}}
                    <div class="form-group row m-b-17 {{ $errors->has('password') ? 'has-error' : ''}}" >
                        <label for="password" class="col-md-2 col-form-label">Password <span class="text-danger">*</span></label>
                        <div class="col-md-7 error-block">
                            {!! Form::text('password', 
                                null,
                                array(
                                    'data-parsley-group'=>'step-member',
                                    'data-parsley-required'=>'true',
                                    'data-parsley-required-message'=>'Password is required',                                           
                                    'placeholder'=>'Password *', 
                                    'id'=>'password',
                                    'data-parsley-pattern'=>"/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/",
                                    'data-parsley-pattern-message'=>'New Password must contain at least 1 uppercase, 1 lowercase, 1 number and 1 special character',                             
                                    'class' => 'form-control', 
                                    'value' => Input::old('password')
                            )) !!}                    
                            @foreach ($errors->get('password') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    {{-- Roles --}}
                    <div class="form-group row m-b-17 {{ $errors->has('role') ? 'has-error' : ''}}" >
                        <label for="role" class="col-md-2 col-form-label">Role <span class="text-danger">*</span></label>
                        <div class="col-md-7 error-block">
                            {!!Form::select('role',
                                \Role::all()->pluck('name','id'), 
                                null,
                                array(
                                    'data-parsley-group'=>'step-member', 
                                    'data-parsley-required'=>'true',
                                    'data-parsley-required-message'=>'Please select role',
                                    'data-parsley-trigger'=> 'change focusout',
                                    'value' => Input::old('role'),
                                    'class'=>'form-control',
                                    'data-size'=>'10',
                                    'id'=>'client',
                                    'data-live-search'=>'false',
                                    'data-style'=>'btn-white',
                                    'tabindex'=>'-98',
                                    'placeholder'=>'--Select Role--'
                                    )
                            )!!}                                
                            @foreach ($errors->get('role') as $message) 
                              <p class="help-block">{{$message}}</p>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-sm-2 col-md-7">
                            <button type="submit" class="btn btn-custom" id="submit_info">Create</button>
                        </div>
                    </div>
                </form>
            </div>

          </div>

          
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
@stop

@section('css')

@stop

@section('js')
    <script src="{{asset('vendor/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <script src="{{asset('vendor/parsley/dist/parsley.js')}}"></script>
    <script src="{{asset('js/parsely-image-validation.js')}}"></script>    
    @include('backend.validation.parsely-unique-validation')    

    <script>
        $(function () {
          bsCustomFileInput.init();
        });

        // parsely

        $('#submit_info').on('click', function(e, anchorObject, stepNumber, stepDirection) {
            var res = $('form[name="form"]').parsley().validate('step-member');
            return res;
        });
    </script>

@stop
