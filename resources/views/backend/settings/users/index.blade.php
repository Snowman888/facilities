@extends('adminlte::page')

@section('title_prefix', 'User Lists - ')

@section('content_header')
    <h1>User Lists</h1>
@stop

@section('content')

	<div class="card card-navy">
		<div class="card-header">
			<h3 class="card-title">User Lists</h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
					<i class="fas fa-minus"></i>
				</button>
				<button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
					<i class="fas fa-times"></i>
				</button>
			</div>
		</div>
		<div class="card-body">
			<table id="members" class="table table-bordered table-striped display" nowrap width="100%">
				<thead>
					<tr>         
						<th style="white-space: nowrap;">User ID</th>
						<th style="white-space: nowrap;">First Name</th> 
						<th style="white-space: nowrap;">Last Name</th> 
						<th style="white-space: nowrap;">Email</th> 
						<th style="white-space: nowrap;">Role</th>         
						<th style="white-space: nowrap;" class="no-sort notexport" data-priority="1"></th>                    
					</tr>
				</thead>
				<tbody>
					@foreach($users as $key=> $user)
					<tr>
						<td>{{$user->id}}</td>
						<td>{{$user->first_name}}</td>
						<td>{{$user->last_name}}</td>
						<td>{{$user->email}}</td>
						<td>{{$user->roles->first()->name}}</td>
						<td>							
							<form method="POST" action="{{url('settings/users/delete/'.$user->id)}}" name="delete-user" id="delete-user" enctype="multipart/form-data">
								@csrf
								@method('DELETE')
								<a href="{{url('settings/users/edit/'.$user->id)}}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
								<a href="javascript:;" class="btn btn-danger btn-xs" data-click="swal-danger" ><i class="fa fa-trash-alt"></i></a>
							</form>							
						</td>
					</tr>
					@endforeach           
				</tbody>
				<tfoot>
					<tr>
						<th>User ID</th>             
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Role</th>
						<th data-priority="1"></th>    
					</tr>
				</tfoot>
			</table>
			
		</div>
		<!-- /.card-body -->
		<div class="card-footer">
			
		</div>
		<!-- /.card-footer-->
	</div>
	
@stop

@section('css')
   <link rel="stylesheet" href="{{asset('vendor/datatables/css/dataTables.bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/datatables-plugins/responsive/css/responsive.bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/datatables-plugins/buttons/css/buttons.bootstrap4.min.css')}}">
@stop

@section('js')
	<script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('vendor/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{asset('vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js')}}"></script>

	<script src="{{asset('vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js')}}"></script>
  	<script src="{{asset('vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js')}}"></script>

	<script src="{{asset('js/datatables-extentions.js')}}"></script>
	<script type="text/javascript">
		$(function () {
		   $('#members').DataTable({
		      "responsive": true,
		      "processing": true,
		      "serverSide": false,
		      "fixedHeader": true,        
		      'paging'      : true,
		      'pageLength'  : 10,
		      'lengthChange': true,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'scrollX'     : false,
		      'autoWidth'   : true, 
		
		     	// dom: 'lBfrtip',
		     	// buttons: 
		     	// [
			     // 	{
			     // 		text: '<i class="fas fa-plus-circle"></i> Add User',
			     // 		className: 'btn-sm btn btn-green addservicessBtn',
			     // 		action: function ( e, dt, node, config ) {
			     // 			onclick (window.location.href="{{url('agency/add-user')}}")
			     // 		}
			     // 	}
		     	// ],
		
		   })
		})
	</script>
	<script type="text/javascript">
		$('[data-click="swal-danger"]').click(function(e) {
			var form = $(this).parents('form');
			dangerNotification('Are You Sure?','You are about to delete this user.','question','Delete',form)
		});
	</script>
@stop
