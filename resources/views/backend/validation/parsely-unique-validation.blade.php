<script type="text/javascript">
window.Parsley
.addValidator('unique_email', function (value, requirement) {
    var response = false;   

    $.ajax({       
        url: "{{url('validation/unique-email')}}/"+requirement,
        data: {value: value,_token: '{{ csrf_token() }}'},
        dataType: 'json', 
        type: 'POST',
        async: false,
        success: function(data) {
            response = true;
        },
        error: function(xhr, status, error) {
            response = false;
        }
    });
    
    return response;
    
}, 32)
.addMessage('en', 'name', 'Email exists');
</script>