<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
        <!-- Styles -->
        <style>
           @import url(https://fonts.googleapis.com/css?family=Nunito+Sans);:root{--blue:#0e0620;--white:#fff;--green:#2ccf6d}body,html{height:100%}body{display:flex;align-items:center;justify-content:center;font-family:"Nunito Sans";color:var(--blue);font-size:1em}button{font-family:"Nunito Sans"}ul{list-style-type:none;padding-inline-start:35px}svg{width:100%;visibility:hidden}h1{font-size:7.5em;margin:15px 0;font-weight:700}h2{font-weight:700}.hamburger-menu{position:absolute;top:0;left:0;padding:35px;z-index:2;width:30px;height:22px;border:none;background:0 0;padding:0;cursor:pointer;height:3px;background:#000;width:100%;left:0;top:0;transition:.1s ease-in}& a{display:block;font-size:1.75em;font-weight:700;text-decoration:none;color:inherit;transition:.24s ease-in-out}&[data-state=open]{transform:translateX(0)}.container{margin-top:70px;margin-bottom:70px}
        </style>

        <style type="text/css">
            .btn-custom{color:#fff;background-color:#001f3f;border-color:#001f3f}.btn-custom.active,.btn-custom:active,.btn-custom:focus,.btn-custom:hover,.open .dropdown-toggle.btn-custom{color:#fff;background-color:#001f3fd9;border-color:#001f3fd9}.btn-custom.active,.btn-custom:active,.open .dropdown-toggle.btn-custom{background-image:none}
        </style>

        {{-- Favicon --}}
        <link rel="shortcut icon" href="{{ asset('favicons/favicon.ico') }}" />

    </head>
    <body>
        <main>
            <div class="container">
                <div class="row">
                    @include('errors.template.left')
                    <div class="col-md-6 align-self-center">
                        <h1>@yield('code')</h1>
                        <h2>UH OH! You're lost.</h2>
                        <p>The page you are looking for does not exist.
                          How you got here is a mystery. But you can click the button below
                          to go back to the homepage.
                        </p>
                        <a class="btn btn-custom" href="{{url('/')}}">Lets Go Home</a>
                    </div>
                </div>
            </div>
        </main>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.1.1/gsap.min.js"></script>

        <script type="text/javascript">
            gsap.set("svg",{visibility:"visible"}),gsap.to("#headStripe",{y:.5,rotation:1,yoyo:!0,repeat:-1,ease:"sine.inOut",duration:1}),gsap.to("#spaceman",{y:.5,rotation:1,yoyo:!0,repeat:-1,ease:"sine.inOut",duration:1}),gsap.to("#craterSmall",{x:-3,yoyo:!0,repeat:-1,duration:1,ease:"sine.inOut"}),gsap.to("#craterBig",{x:3,yoyo:!0,repeat:-1,duration:1,ease:"sine.inOut"}),gsap.to("#planet",{rotation:-2,yoyo:!0,repeat:-1,duration:1,ease:"sine.inOut",transformOrigin:"50% 50%"}),gsap.to("#starsBig g",{rotation:"random(-30,30)",transformOrigin:"50% 50%",yoyo:!0,repeat:-1,ease:"sine.inOut"}),gsap.fromTo("#starsSmall g",{scale:0,transformOrigin:"50% 50%"},{scale:1,transformOrigin:"50% 50%",yoyo:!0,repeat:-1,stagger:.1}),gsap.to("#circlesSmall circle",{y:-4,yoyo:!0,duration:1,ease:"sine.inOut",repeat:-1}),gsap.to("#circlesBig circle",{y:-2,yoyo:!0,duration:1,ease:"sine.inOut",repeat:-1}),gsap.set("#glassShine",{x:-68}),gsap.to("#glassShine",{x:80,duration:2,rotation:-30,ease:"expo.inOut",transformOrigin:"50% 50%",repeat:-1,repeatDelay:8,delay:2});const burger=document.querySelector(".burger"),nav=document.querySelector("nav");burger.addEventListener("click",e=>{"closed"===burger.dataset.state?burger.dataset.state="open":burger.dataset.state="closed","closed"===nav.dataset.state?nav.dataset.state="open":nav.dataset.state="closed"});
        </script>
    </body>
</html>
