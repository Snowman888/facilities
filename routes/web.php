<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Backend;


Route::group(array('middleware' => ['auth','web']), function () {
	Route::get('/', [Backend\Dashboard\DashboardController::class, 'index'])->name('dashboard');

	Route::controller(Backend\Settings\ProfileController::class)->group(function () {
		Route::prefix('settings')->group(function () {
		   Route::get('/profile', 'index');
    		Route::post('/profile', 'update');
    		Route::post('/change-password', 'changePassword');
    		Route::delete('/profile/delete', 'delete');
		});    	
	});

	Route::controller(Backend\Settings\UsersController::class)->group(function () {
		Route::group(array('prefix'=>'settings','middleware' => ['role:Super Admin|Admin']),function () {
    		Route::get('/users', 'index');
    		Route::delete('/users/delete/{user_id}', 'delete');
    		Route::get('/users/create', 'create');
    		Route::post('/users/create', 'store');
    		Route::get('/users/edit/{user_id}', 'edit');
    		Route::post('/users/edit/{user_id}', 'update');
		});    	
	});

	Route::controller(Backend\Core\MasterJobsController::class)->group(function () {
		Route::group(array('prefix'=>'master-jobs'),function () {
    		Route::get('/list', 'index');
    		Route::get('/list-datatables', 'datatables');
    		Route::get('/create', 'create');
    		Route::post('/create', 'store');
    		Route::get('/edit/{mjob_id}', 'edit');
    		Route::post('/edit/{mjob_id}', 'update');
    		Route::delete('/delete/{mjob_id}', 'delete');
		});    	
	});

	// Project Files
	Route::resource('master-jobs.project_files', Backend\Core\ProjectFilesController::class)->except(['show']);
	Route::get('master-job/{mjob_id}/project_files/list-datatables', [Backend\Core\ProjectFilesController::class, 'datatables']);

	// Equipments
	Route::resource('equipments', Backend\Core\EquipmentController::class)->except(['show']);
	Route::get('/equipments/list-datatables', [Backend\Core\EquipmentController::class, 'datatables']);

	// organization
	Route::resource('organizations', Backend\Core\OrganizationController::class)->except(['show']);
	Route::get('/organizations/list-datatables', [Backend\Core\OrganizationController::class, 'datatables']);


	// Contacts
	Route::resource('contacts', Backend\Core\ContactController::class)->except(['show']);
	Route::get('/contacts/list-datatables', [Backend\Core\ContactController::class, 'datatables']);

	Route::controller(Backend\Validation\ParselyValidation::class)->group(function () {
		Route::prefix('validation')->group(function () {
    		Route::post('/unique-email/{user_id}', 'validate_email')->name('validate.email');
    		Route::post('/validateoptions', 'validateoptions')->name('validate.option');
		});    	
	});

});

Auth::routes(['register' => false]);


